+Lunar Lander Agent
Mitchell Kelley
CS7642 Project 2

[github link](https://github.gatech.edu/mkelley32/CS7642_project2)

how to run this code:
just call the script "initial.py" with python3

python3 initial.py

You may need to install some dependencies
(pip3 gave me trouble with permissions, but the --user flag fixed that.)
