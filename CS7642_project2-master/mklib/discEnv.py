
class DiscreteEnv:
    """A class for storing information about the discrete environment"""
    def __init__(self, nDims):
        self.nDims = nDims
        #self.statesByDim

    def tmp(self):
        return 1

    def getPossibleStateCount(self):
        count = 1;
        for b in self.bounds:
            count = count * (len(b) + 1);
        return count;

    def createStates(self, bounds):
        self.bounds = bounds
        
    def getStateFromObs(self, obs):
        state = 0
        for i in range(self.nDims):
            active_obs = obs[i]
            active_bounds = self.bounds[i]
            active_state = 0
            if(active_obs <= active_bounds[0]):
                active_state = 0
            elif(active_obs >= active_bounds[-1]):
                active_state = len(active_bounds)
            else:
                active_state = [x for x in range(len(active_bounds)) if active_obs >= active_bounds[x] and active_obs < active_bounds[x+1]][0] + 1
            state *= (len(active_bounds) + 1)
            state += active_state
        return state

    def getBoundsFromState(self, state):
        bounds = []
        for i in range(self.nDims):
            ind = self.nDims - i -1
            bounds.append(state%len(self.bounds[ind]))
            state = state // len(self.bounds[ind])
        return bounds

#    def getDimensionalStateFromObs(self, obs):
#        dimStates = []
#        for i in range(self.nDims):
#            active_obs = obs[i]
#            active_bounds = self.bounds[i]
#            active_state = 0
#            if(active_obs <= active_bounds[0]):
#                active_state = 0
#            elif(active_obs >= active_bounds[-1]):
#                active_state = len(active_bounds)
#            else:
#                active_state = [x for x in range(len(active_bounds)) if active_obs >= active_bounds[x] and active_obs < active_bounds[x+1]][0] + 1
#            dimStates.append(active_state)
#        return dimStates
