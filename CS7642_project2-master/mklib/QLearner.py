import pprint
import random
import numpy
from scipy import stats
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import time
from PIL import Image

pKnownStates = False
plotting = True
pReward = False
debugG = False
visualizeWeights = False
plotStateFrequencies = True

class QLearner:
    """A class for conducting q learning"""
    def __init__(self, n_actions, n_states):
        self.transitions = dict() # counter for transitions between states
        self.stateVisits = dict() # counter visits to each state
        self.stateActionVisits = dict() # counter for times an action was taken in a state
        self.actions = [0,1,2,3] # each of the possible actions
        self.na = n_actions
        self.ns = n_states
        #self.stateActionValue = [[0.0 for x in range(self.na)] for y in range(self.ns)]
        self.eps = 0.3
        self.gamma = 0.9
        self.alpha = 0.99
        self.w = [0.0 for x in range(self.ns)]
        self.ww = [] # record of all weights
        self.v = [0.0 for x in range(self.ns)]
        self.pp = pprint.PrettyPrinter(depth=6)
        self.episodeId = 0
        self.trialId = 0;
        self.episodeStepCount = []
        #self.nSteps = 8 
        self.nSteps = 1
        #self.recentStates = [0 for x in range(self.nSteps)]
        #self.recentRewards = [0 for x in range(self.nSteps)]
        self.rewards = []
        self.netReward = 0
        self.episodeStateDiscoveryCount = []
        self.nNewStates = 0;
        self.episodeStateActionDiscoveryCount = []
        self.nNewStateActions = 0;
        self.episodeTakeBestActionCount = []
        self.episodeAlpha = []
        self.nTakeBestActions = 0;
        #self.targetMinActionCount = 200
        self.targetMinActionCount = 2
        self.episodeStates = []
        self.episodeRewards = [];
        #self.rewardHistory = []
        self.actionSelectionRatio = 4 # maximum factor by which the number of times the best expected action is allowed t exceed any other actions
        self.alphaDecayRate = 0.9

    def reportHyperParameters(self):
        return "e: {}, g: {}, a: {}, ns: {}".format(self.eps, self.gamma, self.alpha, self.nSteps)

    def startNextEpisode(self):
        self.episodeStepCount.append(self.trialId)
        self.trialId = 0;
        self.episodeId += 1
        #self.recentStates = [0 for x in range(self.nSteps)]
        #self.recentRewards = [0 for x in range(self.nSteps)]
        self.rewards.append(self.netReward)
        if(pReward):
            print("nr: {}".format(self.netReward))
        self.netReward = 0
        self.episodeStateDiscoveryCount.append(self.nNewStates)
        self.nNewStates = 0;
        self.episodeStateActionDiscoveryCount.append(self.nNewStateActions);
        self.nNewStateActions = 0;
        self.episodeTakeBestActionCount.append(self.nTakeBestActions);
        self.episodeAlpha.append(self.alpha);
        self.nTakeBestActions = 0;
        self.ww = self.ww + self.w
        self.episodeStates = []
        self.episodeRewards = [];
        #print(self.w)
        #self.ww.append(self.w)
        #if(self.episodeId > 2000):
        #    targetMinActionCount = 2
        if(self.episodeId%100 == 0):
            self.alpha = self.alpha*.9
            if(self.alpha < 0.2):
                self.alpha = self.alphaDecayRate
            self.eps = self.eps*0.999
            if(self.eps < 0.01):
                self.eps = 0.2
            #self.eps = self.eps*.5
            print("alpha now: {}".format(self.alpha))
        # RESET reference data
        #if(self.episodeId%500 == 0):
        #    print('RESET REF DATA')
        #    self.transitions = dict() # counter for transitions between states
        #    self.stateVisits = dict() # counter visits to each state
        #    self.stateActionVisits = dict() # counter for times an action was taken in a state
        #print("start episode: {}".format(self.episodeId))

    def incrementTrial(self):
        self.trialId += 1

    def createBaseTransition(self, s1, a, s2):
        self.stateVisits[s1] = 0
        self.transitions[s1] = dict()

    def createBaseAction(self, s1, a):
        self.transitions[s1][a] = dict()
        if(s1 not in self.stateActionVisits):
            self.stateActionVisits[s1] = dict()
        self.stateActionVisits[s1][a] = 0

    def incrementTransitions(self, s1, a, s2):
        # make connections if needed
        if(s1 not in self.transitions):
            self.createBaseTransition(s1, a, s2)
        if(a not in self.transitions[s1]):
            self.createBaseAction(s1, a)
        if(s2 not in self.transitions[s1][a]):
            self.transitions[s1][a][s2] = 0
        # increment counts
        self.transitions[s1][a][s2] += 1
        self.stateActionVisits[s1][a] += 1
        self.stateVisits[s1] += 1

    def appendTransition(self, s1, a, s2):
        self.incrementTransitions(s1, a, s2)
        if(s1 in self.transitions):
            if(a in self.transitions[s1]):
                self.incrementTransitions(s1, a, s2)
            else:
                self.createBaseAction(s1, a);
        else:
            self.createBaseTransition(s1, a, s2)
            self.incrementTransitions(s1, a, s2)

    def printTransitions(self):
        pp = pprint.PrettyPrinter(depth=6)
        pp.pprint(self.transitions)
        pp.pprint(self.stateVisits)
        pp.pprint(self.stateActionVisits)
        return
#        for t, actions in self.transitions.items():
#            print(t)
#            print(actions.keys())
#            print(actions.values())
#            for a, targets in actions.items():
#                print(a)
#                print(targets.keys())
#                print(targets.values())

#    def getExpectedNextStates(self, s1):
#        #possibleStates = dict()
#        #print(s1)
#        self.pp.pprint(self.transitions)
#        for _a in self.transitions[s1].keys():
#            for _s2 in self.transitions[s1][_a].keys():
#                if(_s2 in possibleStates):
#                    possibleStates[_s2] += self.transitions[s1][_a][_s2]
#                else:
#                    possibleStates[_s2] = self.transitions[s1][_a][_s2]
#        return possibleStates

    def getApproximateValue(self, state):
        return self.w[state]

    def pickAction(self, s1):
        action = 0;
        if(s1 not in self.transitions):
            self.nNewStates += 1
            if(pKnownStates):
                print("state not present yet")
            return random.choice(self.actions)
        actionCount = dict() 
        minActionCount = 1000000000;
        minAction = 0;
        for a in self.actions:
            # if any action in this state hasn't been tried, take it
            if(a not in self.transitions[s1]):
                return a
            else:
                actionCount[a] = 0
                for s2 in self.transitions[s1][a]:
                    actionCount[a] += self.transitions[s1][a][s2]
                if(actionCount[a] < minActionCount):
                    minActionCount = actionCount[a]
                    minAction = a
        #print(minActionCount)
        if(minActionCount < self.targetMinActionCount): # make sure each action is taken at least this many times
            self.nNewStateActions += 1
            return minAction
        # choose a random action, at 1-epsilon probability
        if(random.random() < self.eps):
            return random.choice(self.actions)
        # pick action with highest expected value
        # as long as it hasn't been selected more than 10x as many times as the least frequent action
        EV = -10000;
        actionCounts = [[] for acts in self.actions]
        for a in self.actions:
            currentEV = 0
            actionCount = 0
            for s2,numberOfOccurences in self.transitions[s1][a].items():
                currentEV += self.getApproximateValue(s2)*numberOfOccurences
                actionCount += numberOfOccurences
            currentEV = currentEV/actionCount
            actionCounts[a] = actionCount
            if(currentEV >= EV):
                EV = currentEV
                action = a
        minActionCount = min(actionCounts)
        if(minActionCount*self.actionSelectionRatio < actionCounts[action]):
            minAction = 0
            for aCounts in actionCounts:
                if(aCounts == minActionCount):
                    return minAction
                minAction += 1
        self.nTakeBestActions += 1;
        return action

    def reportValues(self):
        self.pp.pprint(self.v)

    # n-Step reward
    def getGNonTerminal(self):
        G = 0
        for i in range(min(self.trialId, self.nSteps)):
            G += pow(self.gamma, i)*self.episodeRewards[self.trialId - self.nSteps + i]
        G += pow(self.gamma, self.nSteps) * self.getApproximateValue(self.episodeStates[self.trialId ])
        return G
    def getGTerminal(self,offset):
        G = 0
        for i in range(min(self.trialId, self.nSteps-offset)):
            G += pow(self.gamma, i)*self.episodeRewards[self.trialId + i + offset-self.nSteps]
            #print(G)
        return G

    # this might need to update to handle updating multiple states if I implement tiling
    def observeReward(self, s1, action, s2, reward, done):
        #self.rewardHistory.append(reward)
        self.netReward += reward
        self.appendTransition(s1, action, s2)
        self.episodeRewards.append(reward)
        self.episodeStates.append(s2)
        # apply updates
        nStepMethod = False #seems to have a bug
        if(nStepMethod):
            tau = self.trialId - self.nSteps
            if(not done):
                if(tau >= 0):
                    G = self.getGNonTerminal();
                    if(G > 130):
                        print("Good G (pre): {}".format(G))
                    if(debugG):
                        print("g: {}".format(G))
                    updState = self.episodeStates[tau]
                    self.w[updState] += self.alpha*(G - self.getApproximateValue(updState))
                    #print(self.trialId)
                    #print(s1,s2)
                    #print(self.recentStates)
            else: # we are done and need to close out the recentStates buffer
                if(tau >= 0):
                    #if(debugG):
                        #print(self.recentStates)
                        #print(self.recentRewards)
                    for i in range(self.nSteps):
                        offset = i
                        G = self.getGTerminal(offset);
                        if(debugG):
                            print("G: {}".format(G))
                        #updState = self.recentStates[(tau+offset+1)%self.nSteps]
                        updState = self.episodeStates[tau+offset]
                        if(G > 130):
                            print("Good G(post): {}".format(G))
                        self.w[updState] += self.alpha*(G - self.getApproximateValue(updState))
        else:
            self.w[s1] = (1-self.alpha)*self.w[s1] + self.alpha*(reward + self.gamma*self.w[s2])

        self.incrementTrial()
        return

    def generateResults(self):
        nrewards = len(self.rewards)
        xvals = range(nrewards)
        #print(nrewards, ', ', len(xvals))
        if(len(self.rewards) > 1):
            print(self.reportHyperParameters())
            analysis = stats.linregress(xvals, self.rewards)
            print(analysis)
            if(self.episodeId > 50):
                tenthRewards = nrewards//10
                meanLastTenth = numpy.mean(self.rewards[nrewards-tenthRewards:])
                print("val: {} among last {}".format(meanLastTenth, tenthRewards))
        if(plotting):
            #plt.plot(xvals,self.rewards,
            #    xvals, self.episodeStateDiscoveryCount,
            #    xvals, self.episodeStateActionDiscoveryCount,
            #    xvals, self.episodeTakeBestActionCount)
            plt.clf()
            plt.plot(xvals,self.rewards,label='reward')
            plt.plot(xvals, self.episodeStateDiscoveryCount,label='nStateD')
            plt.plot(xvals, self.episodeStateActionDiscoveryCount, label='nStateActionD')
            plt.plot(xvals, self.episodeStepCount, label='episodeStepCount')
            plt.plot(xvals, self.episodeTakeBestActionCount, label='bBestAction')
            plt.plot(xvals, self.episodeAlpha, label='alpha')
            axes = plt.gca()
            axes.set_ylim([-300,300])
            plt.legend()
            plt.ylabel('some numbers')
            plt.xlabel(self.reportHyperParameters())
            plt.savefig('plots/line/testplot{}.png'.format(time.time()))
            #plt.show()
        numStatesVisited = len(self.stateVisits)
        print(numStatesVisited)
        if(visualizeWeights):
            #print(self.ww)
            translate = interp1d([min(self.ww), max(self.ww)], [0,256])
            self.ww = translate(self.ww)
            intww = [int(val) for val in self.ww]
            wwarr = numpy.array(intww)
            imW = int(len(self.w))
            imH = int(len(self.ww)/len(self.w))
            print(imH, imW)
            wwarr.resize(imH, imW)
            im = Image.fromarray(wwarr.astype('uint8'))
            im.show()
        if(plotStateFrequencies):
            plt.clf()
            plt.plot([k for k in self.stateVisits.keys()], [v for v in self.stateVisits.values()],'*b', label='count')
            plt.plot([k for k in self.stateVisits.keys()], [self.w[k] for k in self.stateVisits.keys()],'*r', label='weights')
            plt.legend()
            plt.ylabel('number of visits')
            plt.xlabel('state')
            #plt.show()
            plt.savefig('plots/scatter/scatterplot{}.png'.format(time.time()))
        #plt.clf()
        #plt.plot(self.rewardHistory)
        #plt.show()


