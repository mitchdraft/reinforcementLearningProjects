

Just realized and issue with using Q-matrix: state transitions are not well defined. Oh, nevermind, they are, but most of the time you will stay in the same state, if the grid is coarse.
Might have to do something to represent the probability of transitioning from one state to another

Multi-stage learning?
Achieve stability in a subset of the dimensions, then pursue max reward in environment?

Tiling, hashing
Bias towards regions of interest


Complications:
the state space is continuous, it is not possible to represent all states indepentdently. Therefore, we need to discretize states into bins.
since our bins are aggregations of states, it is possible that our actions will have a high likelihood of staying in the same state. Therefore, it may be helpful to aggregate the effect of multiple actions



parameters:
	alpha - learning rate
	gamma - discount rate
	n - step size
	mapping/tiling function
Keep track of:
	value matrix, V(x,a)
	weight vector, w (same size as x)
	transition matrix: number of times state x_i transitioned to state x_j
	number of times each state x_i was visited

Algorithm:
	Discretize the state s -> x such that dim(x) = d << dim(s)
	Initialize weight vector to zero, d-length, same as x
	Initialize xTransition matrix, dimension a*dxd, to zero
	Initialize xCount matrix, dimension d, to zero
	Initialize value matrix, dimension dxdim(a), to zero

	c = 0;
	cc = 0;
	while state != terminal
		while cc < n:
			a = chooseAction(x)
			(sp,r,done) = observe(a)
			R[c%n] = r
			S[c%n] = getX(sp)
			c++;
			cc++;
