import unittest
from mklib.discEnv import DiscreteEnv

class TestDiscreteEnv(unittest.TestCase):
    

    def test_tmp(self):
        #de = DiscreteEnv(6)
        #de = 1
        self.assertEqual(1,1)
        #self.assertEqual(de,1)
        #self.assertEqual(de.tmp(),1)


class TestStringMethods(unittest.TestCase):

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

if __name__ == '__main__':
    unittest.main()
