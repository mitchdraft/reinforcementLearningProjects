import gym
import sys
import random
from mklib.discEnv import DiscreteEnv
from mklib.QLearner import QLearner
import pprint
import matplotlib.pyplot as plt

pSummary = False
pReward = True
plotObs = False
exploreStateUtilization = False

random.seed(1000)
env = gym.make('LunarLander-v2')
s = env.seed(seed=100)
observation = env.reset()
#net_reward = 0
#rewards = []

stateNames = ['x', 'y', 'vx', 'vy', 'ang', 'vang', 'lf', 'rf'];

class EnvDiagnostic:
    """A class for understanding state boundaries"""
    def __init__(self):
        # boundaries include min, max, avg
        self.bounds = [[0.0 for x in range(3)] for y in range(len(stateNames))]
        self.nObs = 0
    def applyObs(self, obs):
        self.nObs += 1
        for i in range(len(stateNames)):
            if(obs[i] < self.bounds[i][0]):
                self.bounds[i][0] = obs[i]
            if(obs[i] > self.bounds[i][1]):
                self.bounds[i][1] = obs[i]
            self.bounds[i][2] += obs[i] # just track sum, take mean on call for report
    def reportDiagnostics(self):
        print("diagnostic summary")
        calcBounds = list(self.bounds)
        for i in range(len(calcBounds)):
            calcBounds[i][2] = calcBounds[i][2]/self.nObs
            print('{}: {}'.format(stateNames[i], calcBounds[i]))

envD = EnvDiagnostic();
stateBoundsV0 = [
    [-1,-.25,.25,1],
    [0,.2,.5],
    [-.2,-.05,.05,.2],
    [-.5,-.2,0,.1],
    [-.4,-.1,.1,.4],
    [-.4,-.1,.1,.4],
]
stateBoundsV1 = [
    [-.1,0,.1], #x
    [0,.1,.3], #y
    [-.1,0,.1], #vx
    [-.1,0,.1], #vx
    [-.1,0,.1], #ang
    [-.1,0,.1], #vang
]
stateBoundsV2 = [
    [-.1,.1], #x
    [0,.2], #y
    [-.1,.1], #vx
    [-.1,.1], #vx
    [-.1,.1], #ang
    [-.1,.1], #vang
]
stateBoundsV3 = [
    [-.1,.1], #x
    [0,.3], #y
    [-.1,.1], #vx
    [-.1,.1], #vx
    [-.1,.1], #ang
    [-.1,.1], #vang
]
stateBoundsV4 = [ #-126
    [-.1,.1], #x
    [0,.2], #y
    [-.2,.2], #vx
    [-.1,.1], #vy
    [-.1,.1], #ang
    [-.1,.1], #vang
]
stateBoundsV5 = [
    [-.1,.1], #x
    [0,.2], #y
    [-.2,.2], #vx
    [-.1,.1], #vy
    [-.1,.1], #ang
    [-.1,.1], #vang
]
stateBoundsV6 = [
    [-.1,.1], #x
    [.1,.3, .6], #y
    [-.2,.2], #vx
    [-1,-.2], #vy
    [-.1,.1], #ang
    [-.1,.1], #vang
]
stateBoundsV6 = [
    [-.2,-.1,.1,.2], #x
    [.1,.3, .6], #y
    [-.2,.2], #vx
    [-1,-.2], #vy
    [-.1,.1], #ang
    [-.1,.1], #vang
]
stateBoundsV7 = [
    [-.2,-.1, -.5, .5,.1,.2], #x
    [.1,.3,.6, .9], #y
    [-.4,-.2,.2,.4], #vx
    [-1,-.2,0], #vy
    [-.2,-.1,.1,.2], #ang
    [-.2, -.05,.05,.2], #vang
]
stateBoundsV8 = [ #-152
    [-.2,-.1, -.05, .05,.1,.2], #x
    [.1,.2,.3,.6, .9], #y
    [-.4,-.2,.2,.4], #vx
    [-1,-.5,-.2,0], #vy
    [-.2,-.1,-.05,.05,.1,.2], #ang
    [-.2, -.1,-.05,.05,.1,.2], #vang
]
stateBounds = stateBoundsV8

#discEnv = DiscreteEnv(6)
discEnv = DiscreteEnv(len(stateBounds))
discEnv.createStates(stateBounds)
nActions = 4
nPossibleStates = discEnv.getPossibleStateCount();
print("{} possible states".format(nPossibleStates));

qlearn = QLearner(nActions, nPossibleStates)
observationHistory = []

current_state = discEnv.getStateFromObs(observation)
#for _ in range(200000):
#for _ in range(400000):
#for _ in range(200000):
for _ in range(10000):
#for _ in range(300):
  #env.render()
  past_state = current_state
  #action = env.action_space.sample() # your agent here (this takes random actions)
  action = qlearn.pickAction(current_state);
  #action = 2
  #print(action)
  observation, reward, done, info = env.step(action)
  observationHistory.append(observation)
  #print(observation)
  envD.applyObs(observation)
  current_state = discEnv.getStateFromObs(observation)
  #print(current_state)
  #qlearn.appendTransition(past_state, action, current_state) # this is handled by observeReward
  qlearn.observeReward(past_state, action, current_state, reward, done)
  #print(observation)
  #net_reward += reward
  if done:
      observation = env.reset()
      current_state = discEnv.getStateFromObs(observation)
      qlearn.startNextEpisode()
      #if(pReward):
      #    print(net_reward)
      #rewards.append(net_reward)
      net_reward = 0
  #print(info)

if(pSummary):
    envD.reportDiagnostics()
    qlearn.printTransitions()
    #qlearn.getExpectedNextStates(6267)
    #qlearn.getExpectedNextStates(6888)
    #qlearn.reportValues()

qlearn.generateResults()
#print(discEnv.getBoundsFromState(199))
#print(discEnv.getBoundsFromState(1342))
#print(discEnv.getBoundsFromState(122))
#print(discEnv.getBoundsFromState(123))
if(exploreStateUtilization):
    emptyStates = [k for k in qlearn.stateVisits.keys() if qlearn.stateVisits[k] == 0]
    minStates = [v for v in qlearn.stateVisits.values() if v < 10]
#print(emptyStates)
    print(len(emptyStates))
#print(minStates)
    print(len(minStates))
    absentStates = set(range(nPossibleStates)) - set(qlearn.stateVisits.keys())
#print(absentStates)
    print(len(absentStates))
    cs = [[0 for j in range(6)] for i in range(discEnv.nDims)]
    for s in absentStates:
        discForm = discEnv.getBoundsFromState(s)
        i = 0
        for p in discForm:
            cs[i][p] += 1
            i += 1
    pp = pprint.PrettyPrinter(depth=6)
    pp.pprint(cs)

if(plotObs):
    plt.clf()
#plt.plot(observationHistory)
    f, axarr = plt.subplots(3,2)
    axarr[2,0].hist([x[5] for x in observationHistory], label="vang")
    axarr[2,0].set_title("vang")
    axarr[2,1].hist([x[4] for x in observationHistory], label="ang")
    axarr[2,1].set_title("ang")
    axarr[0,0].hist([x[0] for x in observationHistory], label="x")
    axarr[0,0].set_title("x")
    axarr[0,1].hist([x[1] for x in observationHistory], label="y")
    axarr[0,1].set_title("y")
    axarr[1,0].hist([x[2] for x in observationHistory], label="vx")
    axarr[1,0].set_title("vx")
    axarr[1,1].hist([x[3] for x in observationHistory], label="vy")
    axarr[1,1].set_title("vy")
    plt.show()
