
Procedure:
Make learning agent using any RL method from class to solve Lunar Lander
Upload code to private repo
Make a README.md
-Include instructions for running the source code
-Include link to private repo
Create graphs
-Reward per training episode for each training episode while training the agent
-Reward per trial for 100 trials using the fully trained agent
-The effect of the hyper parameters (alpha, lambda, epsilon)
--You pick the ranges
--Explain why you chose these ranges
-Anything else appropriate
Create a video describing your agent and the experiments you ran
-5 minute maximum
-Same critera as paper
Write a paper describing the agent and experiments performed
-3 pages maximum
-Include the graphs
-Discuss your results
-Explain the algorithms you selected and why you did
-Describe any problems you encountered
-Explain your experiments
-Include a link to your presentation
-Do not include a link to your code
Create another README.txt to link to your code


Code

Performance summary


Paper

Video



