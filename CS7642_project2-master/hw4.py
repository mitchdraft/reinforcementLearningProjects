# Working Solution

# variants of the example
# 0 - south
# 1 - north
# 2 - east
# 3 - west
# 4 - pickup
# 5 - dropoff
import gym
import sys
import random

env = gym.make("Taxi-v2")

random.seed(1000)
eps = 0.9 # threshold for choosing random action, rather than optimal action
eps_red_rate = .99999
gamma = 0.9
alpha = 0.1

pSteps = False;
pDiff = True;
pQTable = True;
tStates = set();

ktStates = set([16, 97, 418, 479])

# initialize matrix to zero
ns, na = 500, 6;
q = [[0.0 for x in range(na)] for y in range(ns)]
T = [[-1 for x in range(na)] for y in range(ns)]
R = [[0.0 for x in range(na)] for y in range(ns)]

actionArray = [0,1,2,3,4,5]
def getAction(state):
  if(random.random() < eps):
    return random.choice(actionArray)
    #return env.action_space.sample() # return random action
  options = q[state]
  maxVAtState = max(options)
  nEquivalentMaxes = q[state].count(maxVAtState)
  if(nEquivalentMaxes == 1):
    maxAction = q[state].index(maxVAtState)
    return maxAction
  # otherwise, we have multiple equally good actions, choose one at random
  inds = [i for i, x in enumerate(q[state]) if x == maxVAtState]
  #print inds;
  return random.choice(inds)

def estimateFutureValue(sp):
  futureStates = set()
  for act in actionArray:
    futureStates.add(T[sp][act])
  if(-1 in futureStates):
      print "need to expand T mat"
      sys.exit()
  futVal = 0.0
  for sf in futureStates:
      futVal += max(q[sf])
  futVal *= gamma
  return futVal

terminalstate = -1 # TODO figure out what the terminal state is
def generateT():
  stepCount = 0;
  observation = env.reset()
  done = False
  reward = 0
  while(reward != 20 and stepCount < 200):
    action = getAction(observation);
    oldObs = observation
    observation, reward, done, info = env.step(action)
    # build Transition matrix
    T[oldObs][action] = observation
    R[oldObs][action] = reward
    stepCount += 1
def launchEpisode():
  stepCount = 0;
  observation = env.reset()
  if observation in ktStates:
      print "started on terminal state"
      sys.exit();
  #print observation;
  if(observation == terminalstate):
    return
  done = False
  #while(not done and stepCount < 300): # must be internal limit of 200 loops
  reward = 0
  while(reward != 20 and stepCount < 200):
    #env.render()
    action = getAction(observation);
    oldObs = observation
    observation, reward, done, info = env.step(action)
    # update the value of the <s,a>
    #q[oldObs][action] += R[oldObs][action]
    #q[oldObs][action] *= estimateFutureValue(oldObs)
    if(reward != 20):
      q[oldObs][action] = (1-alpha)*q[oldObs][action]
      q[oldObs][action] += alpha*reward
      q[oldObs][action] += alpha*gamma*max(q[observation])
    else:
      q[oldObs][action] = (1-alpha)*q[oldObs][action]
      q[oldObs][action] += alpha*reward
      #q[oldObs][action] = reward

      tStates.add(observation)

    stepCount += 1
  if(pSteps):
    print stepCount


#Q(462, 4) = -11.374402515
#Q(398, 3) = 4.348907
#Q(253, 0) = -0.5856821173
#Q(377, 1) = 9.683
#Q(83, 5) =  -12.8232660372


# debug (long) always use same random path
s = env.seed(seed=100L)
for _ in range(500):
  generateT()
for loop in range(50000):
  eps *= eps_red_rate
  if loop == 1000:
      #print eps
      eps = 0.8;
  if loop == 4000:
      #print eps
      eps = 0.8;
  if loop == 9000:
      #print eps
      eps = 0.8;
  if loop == 12000:
      #print eps
      eps = 0.7;
  #print q[462][4] +11.374402515
  launchEpisode()
if pQTable:
  for i in range(ns):
      print i
      print q[i]
#for i in range(ns):
#    print T[i]
if pDiff:
  print q[462][4]
  print q[398][3]
  print q[253][0]
  print q[377][1]
  print q[83][5] 
  print "Diffs:"
  print q[462][4] +11.374402515
  print q[398][3] -4.348907
  print q[253][0] +0.5856821173
  print q[377][1] -9.683
  print q[83][5]  +12.8232660372


#print tStates
