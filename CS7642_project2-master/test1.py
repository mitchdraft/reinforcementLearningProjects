import unittest
from mklib.discEnv import DiscreteEnv

class TestDiscreteEnv(unittest.TestCase):

    bounds = [[1,2,3],
        [2,3],
        [3,4,5]]

    def test_tmp(self):
        de = DiscreteEnv(6)
        self.assertEqual(1,1)
        self.assertEqual(de.tmp(),1)

    def test_getStateFromObs(self):
        de = DiscreteEnv(3)
        de.createStates(self.bounds)
        states = de.getStateFromObs([0,0,0])
        self.assertEqual(states,0)
        states = de.getStateFromObs([0,0,100])
        self.assertEqual(states,3)
        states = de.getStateFromObs([0,0,3.5])
        self.assertEqual(states,1)
        states = de.getStateFromObs([0,0,4.5])
        self.assertEqual(states,2)
        states = de.getStateFromObs([0,2.5,0])
        self.assertEqual(states,4)
        states = de.getStateFromObs([2.5,0,0])
        self.assertEqual(states,2*3*4)
        states = de.getStateFromObs([2.5,2.5,100])
        self.assertEqual(states,2*3*4 + 1*4 + 3)
        states = de.getStateFromObs([100,100,100])
        self.assertEqual(states,3*3*4 + 2*4 + 3)



if __name__ == '__main__':
    unittest.main()
