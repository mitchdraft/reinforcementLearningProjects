#include <iostream>
#include <vector>
#include <unordered_map>
#include <cassert>
#include <fstream>
#include <string>
#include <numeric>
#include <math.h>
#include <unordered_map>

using namespace std;

bool debugA = false;
bool debugB = false;
bool debug_print_episodes = false;
bool debug_print_history = false;

ofstream logFile;
bool randLeft(){
	if(rand() > RAND_MAX/2){
		return true;
	}
	return false;
}

vector<double> addVectors(vector<double> a, vector<double> b){
	vector<double> c;
	for(int i = 0; i < a.size(); i++){
		c.push_back(a[i] + b[i]);
	}
	return c;
}

vector<double> scaleVector(vector<double> a, double s){
	vector<double> c;
	for(int i = 0; i < a.size(); i++){
		c.push_back(a[i] * s);
	}
	return c;
}

void printVector(vector<double> a, string s, string sep){
	cout << s;
	for(int i = 0; i < a.size(); i++){
		cout << a[i] << sep;
	}
	cout << endl;
}
void printMatrix(vector<vector<double> > a, string s, string sep){
	cout << s;
	for(int i = 0; i < a.size(); i++){
		printVector(a[i], "", ",");
		cout << sep;
	}
	cout << endl;
}

vector<double> vectorState(int p){
	vector<double> state = {0,0,0,0,0};
	state[p-1] = 1.0;
	if(debugA)
		printVector(state, "st: ", ",");
	return state;
}

// walk left or right until hitting a terminal state
// states: A B C D E F G
//         0 1 2 3 4 5 6
// return 0 if termial state is 'A' return 1 if terminal state is 'G'

int nextState(int state){
	if( randLeft() ){
		return --state;
	}
	return ++state;
}

void reportState(int state){
	const char states[] = "ABCDEFG";
	cout << states[state];
}

// note generalized states are vectors. Since the states in this problem are vectors consiting of all zeros except for a single "1" we can treat the state as a scalar value which indicates the position of the non-zero element
int takeWalk(int startValue, bool printOut){
	assert(startValue >= 0 && startValue <= 6);
	int state = startValue; // start at state 'D'
	while(state > 0 && state < 6) {
		if(printOut)
			reportState(state);
		state = nextState(state);
	}
	int score = (state == 0) ? 0 : 1; // states A and G, respectively
	if(printOut){
		reportState(state);
		cout << " return value: " << score << endl;
	}
	return score;
}

vector<int> generatePath(){
	vector<int> path;
	int state = 3;
	while(state > 0 && state < 6){
		path.push_back(state);
		state = nextState(state);
	}
	// get the terminal value
	path.push_back(state);
	return path;
}
vector<vector<int> > generatePaths(int nPaths){
	vector<vector<int> > paths;
	for(int i = 0; i < nPaths; i++){
		paths.push_back(generatePath());
	}
	return paths;
}
vector<vector<vector<int> > > generateData(int nPaths, int nTrials){
	vector<vector<vector<int> > > data;
	for(int i = 0; i < nTrials; i++){
		data.push_back(generatePaths(nPaths));
	}
	return data;
}

// run a bunch of walks and get the average result, compare to analytical solution
void test_takeWalk(){
	cout << ">>> Testing takeWalk function, starting at 'D':" << endl;
	for(int i = 0; i<5; i++){
		takeWalk(3, true);
	}
	const double trials = 1000; // error will decrease as number of trials increases
	cout << ">>> Testing takeWalk function with " << trials << " trials" << endl;
	for(int j = 0; j <= 6; j++){
		double sum = 0;
		for(int i = 0; i<(int)trials; i++){
			sum += takeWalk(j, false);
		}
		double ratio = sum/trials;
		double expected = (double)j/6; // this is the analytical solution
		cout << "starting at " << j << ", average value: " << ratio << " error: " << abs(expected-ratio) << endl;
	}
}

// check distribution of the randLeft function
void test_randLeft(){
	cout << ">>> Testing randLeft function, expecting roughly even distribution." << endl;
	unordered_map<int,int> dist;;
	cout << "rand max: " << RAND_MAX << endl;
	cout << (randLeft() ? "left" : "right") << endl;
	for(int i = 0; i<10000; i++){
		++dist[randLeft() ? 1 : 0];
	}
	cout << "left:  " << dist[1] << endl;
	cout << "right: " << dist[0] << endl;
}

class TDModel {
	public:
	double lambda;
	double alpha;
	double convergenceTarget = 0.0001;
	//double convergenceTarget = 0.00002;
	ofstream ofs;
	bool logging = true;
	vector<double> weights;
	vector<double> startingWeights;
	vector<double> predictions;
	TDModel(double lambda, double alpha){
		this->lambda = lambda;
		this->alpha = alpha;
	}
	vector<double> results;
	vector<double> error;
	vector<vector<double> > states;

	void printSummary(){
		cout << "TDModel summary:" << endl;
		cout << "lambda: " << this->lambda << endl;
		cout << "alpha: " << this->alpha << endl;
		if(this->logging){
			//logFile << "TDModel summary:" << endl;
			logFile << "l: " << this->lambda << " a: " << this->alpha << ", ";
		}
	}


	// initialize the weights
	void seedWeights(vector<double> ws){
		this->weights = ws;
		this->startingWeights = ws;
	}
	void resetWeights(){
		this->weights = this->startingWeights;
	}
	void resetResults(){
		this->results = {};
		this->error = {};
	}
	void resetStates(){
		this->states = {};
		this->states.push_back(this->startingWeights);
	}

	// this give the correct error for when alpha = 0, according to plot 4
	double calculateError(vector<double> w){
		vector<double> error;
		double varsum = 0;
		for(int j = 0; j < 5; j++){
			varsum += pow((j+1.0)/6.0 - w[j], 2); //true value for middle node is 0.5
		}
		return sqrt(varsum/5);
	}
	void updateWeights(vector<double> dw){
		this->weights = addVectors(this->weights, dw);
	}
	bool checkConvergence(vector<double> prior){
		double maxDiff = 0;
		for(int i = 0; i < this->weights.size(); i++){
			double currentDiff = abs(this->weights[i] - prior[i]);
			if(currentDiff > maxDiff)
				maxDiff = currentDiff;
		}
		if(maxDiff < this->convergenceTarget)
			return true;
		return false;
	}
	void openFile(string filename){
		this->ofs.open(filename, ofstream::out | ofstream::trunc);
	}
	void closeFile(){
		this->ofs.close();
	}
	void printPlot(int index){
		this->ofs << index << ", ";
		for(int i = 0; i < this->weights.size(); i++){
			this->ofs << this->weights[i] << ", ";
		}
		this->ofs << endl;
	}
	void printEpisode(vector<int> session){
		string names = "ABCDEFG";
		for(int i = 0; i < session.size(); i++){
			cout << names.at(session[i]);
		}
		cout << endl;
	}
	void printWeights(){
		cout << "weights: ";
		for(int i = 0; i < this->weights.size(); i++){
			cout << this->weights[i] << ", ";
		}
		cout << endl;
	}
	double printResults(){
		cout << "results: ";
		for(int i = 0; i < this->results.size(); i++){
			cout << this->results[i] << " (" << this->error[i] << ") , ";
		}
		cout << endl;

		double sum = std::accumulate(this->error.begin(), this->error.end(), 0.0);
		double mean = sum / this->error.size();

		double sq_sum = std::inner_product(this->error.begin(), this->error.end(), this->error.begin(), 0.0);
		double stdev = std::sqrt(sq_sum / this->error.size() - mean * mean);

		cout << "mean error: " << mean << " std error: " << stdev << endl;
		if(this->logging){
			logFile << "mErr: " << mean << " stdErr: " << stdev << endl;
		}
		return mean;
	}
	void trainSessionBatch(vector<vector<int> > session){
		vector<double> delta_weight = {0.0,0.0,0.0,0.0,0.0};
		vector<double> prior_weight = this->weights;
		bool searching = true;
		for(int c = 0; c < 10000 && searching; c++){ // bail on convergence
		//for(int c = 0; c < 800; c++){ // do not bail on convergence
			for(int i = 0; i < session.size(); i++){ // set of ten episodes
				vector<double> el = {0.0,0.0,0.0,0.0,0.0};
				this->resetStates();
				for(int j = 1; j < session[i].size()-1; j++){ // set of steps in episode
					int state = session[i][j];
					int nextState = session[i][j+1];
					double pCurrent = this->weights[state-1];
					double pNext;
					if(nextState == 0){
						pNext = 0;
					}
					else if(nextState == 6){
						pNext = 1;
					}
					else{
						pNext = this->weights[nextState-1];
					}
					for(int k = 0; k <= j; k++){
						int updatedState = session[i][k];
						double pUpdated = this->weights[updatedState-1]; // ok because we only update states after running through the whole session
						for(int st = 1; st <= 5; st++){
							if(st == updatedState){
								delta_weight[st-1] += this->alpha*(pNext - pCurrent)*pow(this->lambda,j-k)*pUpdated;
							}
							else {
								delta_weight[st-1] += this->alpha*pow(this->lambda,j-k)*delta_weight[st-1];
							}
						}
					}
				}
			}
			this->updateWeights(delta_weight);
			if(this->checkConvergence(prior_weight)){
				cout << "converged after " << c << " iterations" << endl;
				searching = false;
			}
			prior_weight = this->weights;
			delta_weight = {0,0,0,0,0};
		}
	}
	void trainSessionLive(vector<vector<int> > session){
		vector<double> last_estimate = this->startingWeights;
		vector<double> delta_weight = {0,0,0,0,0};
			for(int i = 0; i < session.size(); i++){ // set of ten episodes
				vector<vector<double> > stateHistory;
				stateHistory.push_back(last_estimate);
				if(debug_print_episodes)
					this->printEpisode(session[i]);
				for(int j = 0; j < session[i].size()-1; j++){ // set of steps in episode
					int state = session[i][j];
					int nextState = session[i][j+1];
					double pCurrent = this->weights[state-1];
					double pNext;
					if(nextState == 0){
						pNext = 0;
					}
					else if(nextState == 6){
						pNext = 1;
					}
					else{
						pNext = this->weights[nextState-1];
					}
					delta_weight = {0,0,0,0,0};
					double newInformation = this->alpha*(pNext - pCurrent);
					vector<double> lambdaSum = {0,0,0,0,0};
					for(int k = 0; k <= j; k++){
						if(debugA)
							cout << k << endl;
						vector<double> scaledPast = scaleVector(vectorState(session[i][k]), pow(this->lambda,j-k));
						lambdaSum = addVectors(lambdaSum, scaledPast);
					}
					delta_weight = scaleVector(lambdaSum, newInformation);
					if(debugA)
						printVector(delta_weight, "dw: ", ",");
					this->updateWeights(delta_weight);
					stateHistory.push_back(this->weights);
				}
				last_estimate = this->weights;
				if(debugA | debug_print_history)
					printMatrix(stateHistory, "StateHistory:\n", "");
				if(debugB)
					this->printWeights();
			}
	}
	void trainBatch(vector<vector<vector<int> > > data){
		vector<double> delta_weight = {0,0,0,0,0};
		for(int i = 0; i < data.size(); i++){
			this->trainSessionBatch(data[i]);
			this->results.push_back(this->weights[2]); //sampling from the middle node
			this->error.push_back(this->calculateError(this->weights)); //true value for middle node is 0.5
			if(debugB)
				this->printWeights();
			this->resetWeights();
		}
		this->printResults();
	}
	double trainLive(vector<vector<vector<int> > > data){
		vector<double> delta_weight = {0,0,0,0,0};
		for(int i = 0; i < data.size(); i++){
			this->trainSessionLive(data[i]);
			this->results.push_back(this->weights[2]); //sampling from the middle node
			this->error.push_back(this->calculateError(this->weights)); //true value for middle node is 0.5
			if(debugB)
				this->printWeights();
			this->resetWeights();
		}
		return this->printResults();
	}
};

void printData(vector<vector<vector<int> > > *data){
	for(int i = 0; i < data->size(); i++){
		cout << "trial " << i << endl;
		for(int j = 0; j < data->at(i).size(); j++){
			cout << "path " << j << " : ";
			for(int k = 0; k < data->at(i).at(j).size(); k++){
				cout << data->at(i)[j][k];
			}
			cout << endl;
		}
	}
}

int main(int argc, char *argv[]){
	srand(atoi(argv[1]));

	int logId = 0;
	string testGoal = "p5. baseline";
	string logFilename = "tmp_log" + to_string(logId) + ".tmp";
	logFile.open(logFilename, ofstream::out | ofstream::trunc);
	logFile << testGoal << endl;


	vector< vector< vector<int> > > data = generateData(10,100);
	

	int plot = 4;
	if(plot == 2){ //testing
		// Tests are passing
		test_randLeft();
		test_takeWalk();

		double sampleLambda = 0.0;
		double sampleAlpha = 0.0;
		TDModel td = TDModel(sampleLambda, sampleAlpha);
		td.printSummary();
		double err = td.calculateError({.5,.5,.5,.5,.5});
		cout << "error is: " << err << endl;
	}
	else if(plot == 3){
		vector<double> plot3lambdas = {0.0, .1, .3, .5, .7, 1.0};
		double plot3alpha = 0.001;
		for(int i = 0; i < plot3lambdas.size(); i++){
			TDModel td = TDModel(plot3lambdas[i], plot3alpha);
			td.printSummary();
			td.seedWeights({.5,.5,.5,.5,.5});
			td.trainBatch(data);
		}
	}
	else if(plot == 4){
		vector<double> plot4lambdas = {0.0, 0.3, 0.8, 1.0};
		vector<double> plot4alpha = {0,.05,.1,.15,.2,.25,.3,.35,.4,.45,.5,.55,.6};;
		for(int i = 0; i < plot4lambdas.size(); i++){
			for(int j = 0; j < plot4alpha.size(); j++){
				TDModel td = TDModel(plot4lambdas[i], plot4alpha[j]);
				td.printSummary();
				td.seedWeights({.5,.5,.5,.5,.5});
				td.trainLive(data);
			}
		}
	}
	else if(plot == 5){
		vector<double> plot5lambdas = {0.0, .05, .1, .15, .2, .25, 0.3, .35, .4, .45, .5, .55, .6, .65, .7, .75, 0.8, .85, .9, .95, 1.0};
		vector<double> plot5alpha = {0,.05,.1,.15,.2,.25,.3,.35,.4,.45,.5,.55,.6};
		unordered_map<double, double> solution; // list of best errors
		unordered_map<double, double> solution_alpha; // list of best errors' alpha value
		for(int j = 0; j < plot5lambdas.size(); j++){
			double current_lambda = plot5lambdas[j];
			for(int i = 0; i < plot5alpha.size(); i++){
				double current_alpha = plot5alpha[i];
				TDModel td = TDModel(current_lambda, current_alpha);
				td.printSummary();
				td.seedWeights({.5,.5,.5,.5,.5});
				double error = td.trainLive(data);
				if(i == 0){
					solution[current_lambda] = error;
					solution_alpha[current_lambda] = current_alpha;
				}
				else{
					if(error < solution[current_lambda]){
						solution[current_lambda] = error;
						solution_alpha[current_lambda] = current_alpha;
					}
				}
			}
		}
		cout << "summary:" << endl;
		for(int i = 0; i < plot5lambdas.size(); i++){
			double lamb = plot5lambdas[i];
			cout << i << ", " << lamb << ", " << solution[lamb] << ", " << solution_alpha[lamb] << endl;
		}
	}

	logFile.close();

	return 0;
}
