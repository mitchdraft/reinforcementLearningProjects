## CS7642 Project 1
- Mitchell Kelley

### How to use:
- compile and run with a random value generator seed: `g++ randomWalk.cpp -o randomWalk.out && ./randomWalk.out 11 `

- print out results: `cat tmp_log0.tmp`

- organize data in excel (I did not include any plotting capabilities in this script)

#### Additional usage notes:
- By default, the script generates the data for plot 4.
- If you would like to generate the data for plots 3 or 5, you can update `randomWalk.cpp` so that `int plot = 3; // 4 // etc.`


