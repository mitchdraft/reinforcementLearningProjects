#include <limits.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "soccer/constants.h"
#include "qlearn/qlearn.cc"
#include "soccer/soccer_arena.cc"
#include "soccer/util.h"
#include "agent/agent.cc"
#include "clp/solver.cc"

using namespace std;

int main(){
	ofstream log_file;
	ofstream q_file;
	int logId = 0;
	string logFilename = "tmp_log" + to_string(logId) + ".tmp";
	log_file.open(logFilename, ofstream::out | ofstream::trunc);
	string q_file_name = "q_file_" + to_string(logId) + ".tmp";
	q_file.open(q_file_name, ofstream::out | ofstream::trunc);

	cout << "Running CE-Q" << endl;

	srand(10); // use random number seed

	//int N_CYCLES = 10000000; // (10^7, as in paper)
	int N_CYCLES = 1000000; // for testing (10^6)
	//int N_CYCLES = 100000; // for testing (10^5)

	vector<float> probe_value = {};
	probe_value.resize(N_CYCLES-1);
	int probe_state = 3;
	int probe_action_a = SA::SOUTH;
	int probe_action_b = SA::STICK;
	float gamma = 0.9;
	float eps = 0.1;
	float alpha = .2;
	//const float EPSILON_REDUCTION_VALUE = .99999;
	const float EPSILON_REDUCTION_VALUE = 1.0;
	//const float ALPHA_REDUCTION_VALUE   = .99999999;
	const float ALPHA_REDUCTION_VALUE   = 1.0;

	cout << "Initializing Soccer Arena" << endl;
	int playerAInitialPosition = 1;
	int playerBInitialPosition = 2;
	bool playerAHasBall = true;
	bool debug_mode = false;
	SA::SoccerArena arena = SA::SoccerArena(
		"testArena",
		playerAInitialPosition,
		playerBInitialPosition,
		playerAHasBall,
		debug_mode
	);

	cout << "Initializing QLearner" << endl;
	const int state_count = SA::N_ACTIVE_STATES;
	const int action_count = SA::N_ACTIONS;
	float initialQLearnerValue = 10.0;
	QLEARN::QLearner ql = QLEARN::QLearner(
		state_count,
		action_count,
		initialQLearnerValue
	);

	cout << "Initializing Agent" << endl;
	AGENT::Agent ag = AGENT::Agent(AGENT::A_CORRELATEDQ, &ql, eps);

	// testing:
	SA::Transition sample_transition = arena.GetTransition(0);
	AGENT::ActionChoice ac = ag.ChooseCeqAction(sample_transition);

	int current_state = ag.GetStartingState();
	float current_diff = 0;
	float last_diff = 0;
	float current_probe_value = 0;
	float last_probe_value = 0;
	int new_game = 1;
	int gc = 0;
	for(int i = 0; i < N_CYCLES; i++){
		// for given state, get action transitions matrix
		SA::Transition tr = arena.GetTransition(current_state);
		// give values to each of the joint actions
		// choose an action using agent
		AGENT::ActionChoice ac = ag.ChooseCeqAction(tr);
		// determine who goes first, p=0.5
		bool a_goes_first = randBool();
		// observe reward and update the Q matrix and current state
		float observed_reward = (a_goes_first)
			? ac.a_first_reward
			: ac.b_first_reward;
		int next_state = (a_goes_first)
			? ac.a_first_next_state
			: ac.b_first_next_state;
		float q_next_state = (next_state >= SA::N_ACTIVE_STATES)
			? 0
			: ag.q_learner_->getValue(current_state, ac.player_b_action, ac.player_b_action);

		float current_state_q_value = ag.q_learner_->getValue(
			current_state,
			ac.player_a_action,
			ac.player_b_action);

		float new_q_value =
			(1-alpha)*current_state_q_value
			+ alpha*(
				(1-gamma)*observed_reward
				+ gamma*ac.anticipated_value
			);

		ag.q_learner_->setValue(
			current_state,
			ac.player_a_action,
			ac.player_b_action,
			new_q_value);

		current_state = next_state;
		// if state is terminal, then reset environment
		if(current_state >= SA::N_ACTIVE_STATES){
			current_state = ag.GetStartingState();
			new_game = 1;
			gc++;
		}
		// filter out places where it does not change value:
		current_probe_value = ag.q_learner_->getValue(probe_state, probe_action_a, probe_action_b);
		if(i>0){
			current_diff = abs(current_probe_value-last_probe_value);
			if(current_diff == 0){
				probe_value[i-1] = last_diff;
			}
			else{
				probe_value[i-1] = current_diff;
				last_diff = current_diff;
			}
		}
		last_probe_value = current_probe_value;
		alpha = alpha * ALPHA_REDUCTION_VALUE;
		eps = eps * EPSILON_REDUCTION_VALUE;
		new_game = 0;
	}
	q_file << ag.q_learner_->getLongSummary().str() << endl;
	current_state = 3;
	SA::Transition tr_trained = arena.GetTransition(current_state);
	AGENT::ActionChoice ac_trained = ag.ChooseCeqAction(tr_trained);
	cout << "After training, chooses action (a,b): (" << ac.player_a_action << ", " << ac.player_b_action << ")" << endl;

	for(int i = 0; i<N_CYCLES; i++){
		log_file << probe_value[i] << endl;
	}
	cout << "Played " << gc << " games" << endl;
	
	log_file.close();
	q_file.close();
	return 0;
}
