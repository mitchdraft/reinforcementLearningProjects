#include <limits.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "soccer/constants.h"
#include "qlearn/qlearn.cc"
#include "soccer/soccer_arena.cc"
#include "soccer/util.h"
#include "agent/agent.cc"
#include "clp/solver.cc"

using namespace std;

int main(){
	cout << "Max int: " << INT_MAX << endl;
	ofstream log_file;
	ofstream q_file;
	int logId = 0;
	string logFilename = "tmp_log" + to_string(logId) + ".tmp";
	log_file.open(logFilename, ofstream::out | ofstream::trunc);
	string q_file_name = "q_file_" + to_string(logId) + ".tmp";
	q_file.open(q_file_name, ofstream::out | ofstream::trunc);

	cout << "Running Friend-Q" << endl;

	srand(10); // use random number seed

	//int N_CYCLES = 10000000; // (10^7, as in paper)
	//int N_CYCLES = 1000000; // for testing (10^6)
	int N_CYCLES = 100000; // for testing (10^5)

	//float probe_value[N_CYCLES];
	// takes 8 seconds for 10^6 (when run on the stack)
	// takes 80 seconds for 10^7 (*must* be on the heap (unless there's a way to increase stack size to the necessary size))
	vector<float> probe_value = {}; // this is necessary to put data on the heap, otherwise it is in the stack, and it will run out of space since there are so many cycles
	probe_value.resize(N_CYCLES-1);
	int probe_state = 3;
	int probe_action_a = SA::SOUTH;
	int probe_action_b = SA::STICK;
	float gamma = 0.9;
	//float gamma = 0.6;
	//float eps = 0.1;
	float eps = 0.2;
	//float alpha = .7;
	float alpha = .008;
	//float alpha = 0.01;
	//float alpha = 1;
	//const float ALPHA_REDUCTION_VALUE = 0.9999995;
	const float EPSILON_REDUCTION_VALUE = .99999;
	const float ALPHA_REDUCTION_VALUE   = .99999999;
	// a: .7, e: .2 worked ok
	// .02,.1,g:.7

	// initialV: 1.0, eps: 0.1, alpha: 0.001, -> converges in same time, but much smoother than in paper
	// initialV: 1.0, eps: 0.001, alpha: 0.001, -> does not converge in 10^6 cycles
	// initialV: 1.0, eps: 0.01, alpha: 0.001, -> converges a bit faster, smooth, but with bumps
	// initialV: 1.0, eps: 0.01, alpha: 0.01, -> converges a lot faster, max error is also greater, gets big, then settles quick

	cout << "Initializing Soccer Arena" << endl;
	int playerAInitialPosition = 1;
	int playerBInitialPosition = 2;
	bool playerAHasBall = true;
	bool debug_mode = false;
	SA::SoccerArena arena = SA::SoccerArena(
		"testArena",
		playerAInitialPosition,
		playerBInitialPosition,
		playerAHasBall,
		debug_mode
	);

	cout << "Initializing QLearner" << endl;
	const int state_count = SA::N_ACTIVE_STATES;
	const int action_count = SA::N_ACTIONS;
	float initialQLearnerValue = 100.0;
	QLEARN::QLearner ql = QLEARN::QLearner(
		state_count,
		action_count,
		initialQLearnerValue
	);

	cout << "Initializing Agent" << endl;
	AGENT::Agent ag = AGENT::Agent(AGENT::A_FRIEND, &ql, eps);

	// testing:
	SA::Transition sample_transition = arena.GetTransition(0);
	//cout << arena.summarizeStateTransitionMap(0).str();
	//ag.q_learner_->setValue(2,1,999);
	//ag.q_learner_->setValue(8,1,9999);
	//cout << ag.q_learner_->getLongSummary().str() << endl;
	AGENT::ActionChoice ac = ag.chooseFriendAction(sample_transition);
	//cout << "action choice: " << ac.player_a_action << ", " << ac.player_a_action << endl;

	// solution outline:
	//  initialize environment, learner, and agent
	//  start game with random initial state (note, this ASSUMPTION may deviate from the paper)
	//  -get a list of valid states
	//  -choose one at random
	//  pick next action
	//  -get a list of possible actions from the environment
	//  -have the qlearner rate the value of the possible next states
	//  -have the agent take an action according to its policy
	//  update Q
	//  repeat until game ends, then restart a new game
	//  repeat until you hit 10 million iterations

	int current_state = ag.GetStartingState();
	float current_diff = 0;
	float last_diff = 0;
	float current_probe_value = 0;
	float last_probe_value = 0;
	int new_game = 1;
	int gc = 0;
	for(int i = 0; i < N_CYCLES; i++){
		// for given state, get action transitions matrix
		SA::Transition tr = arena.GetTransition(current_state);
		// give values to each of the joint actions
		// choose an action using agent
		AGENT::ActionChoice ac = ag.chooseFriendAction(tr);
		// determine who goes first, p=0.5
		bool a_goes_first = randBool();
		// observe reward and update the Q matrix and current state
		float observed_reward = (a_goes_first)
			? ac.a_first_reward
			: ac.b_first_reward;
		int next_state = (a_goes_first)
			? ac.a_first_next_state
			: ac.b_first_next_state;
		float q_next_state = (next_state >= SA::N_ACTIVE_STATES)
			? 0
			: ag.q_learner_->getValue(current_state, ac.player_b_action, ac.player_b_action);
		//cout << "(A,B): " << ac.player_a_action << ", " << ac.player_b_action << endl;

		float current_state_q_value = ag.q_learner_->getValue(
			current_state,
			ac.player_a_action,
			ac.player_b_action);

		float new_q_value =
			(1-alpha)*current_state_q_value
			+ alpha*(
				(1-gamma)*observed_reward
				+ gamma*ac.anticipated_value
			);

		ag.q_learner_->setValue(
			current_state,
			ac.player_a_action,
			ac.player_b_action,
			new_q_value);

		current_state = next_state;
		// if state is terminal, then reset environment
		if(current_state >= SA::N_ACTIVE_STATES){
			//cout << "plus" << endl;
			current_state = ag.GetStartingState();
			new_game = 1;
			gc++;
		}
		//cout << "CS: " << current_state<<endl;
		//cout << ag.q_learner_->getSummary().str() << endl;
		//probe_value[i] = ag.q_learner_->getValue(probe_state, probe_action_a, probe_action_b);
		// filter out places where it does not change value:
		current_probe_value = ag.q_learner_->getValue(probe_state, probe_action_a, probe_action_b);
		if(i>0){
			//probe_value[i-1] = gc;
			//probe_value[i-1] = current_probe_value;
			current_diff = abs(current_probe_value-last_probe_value);
			if(current_diff == 0){
				probe_value[i-1] = last_diff;
			}
			else{
				probe_value[i-1] = current_diff;
				last_diff = current_diff;
			}
		}
		last_probe_value = current_probe_value;
		alpha = alpha * ALPHA_REDUCTION_VALUE;
		eps = eps * EPSILON_REDUCTION_VALUE;
		new_game = 0;
	}
	q_file << ag.q_learner_->getLongSummary().str() << endl;

	for(int i = 0; i<N_CYCLES; i++){
		log_file << probe_value[i] << endl;
	}
	
	log_file.close();
	q_file.close();
	//cout << "Max int: " << INT_MAX << endl;
	return 0;
}
