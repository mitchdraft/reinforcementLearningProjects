#include <string>
#include <iostream>
#include "qlearn.cc"

int main(){
	cout << "Running qlearner sample app" << endl;
	int stateCount = 8;
	int actionCount = 5;
	float initialQLearnerValue = 10.0;
	QLEARN::QLearner ql = QLEARN::QLearner(
		stateCount,
		actionCount,
		initialQLearnerValue
	);
	cout << "Inital Q matrix:" << endl;
	cout << ql.getSummary().str() << endl;

	cout << "Testing the getter and setter:" << endl;
	cout << "getValue(1,1): " << ql.getValue(1,1) << endl;
	cout << "getValue(0,2): " << ql.getValue(0,2) << endl;
	cout << "setValue(0,2,999): " << endl;
	ql.setValue(0,2,999);
	cout << "getValue(0,2): " << ql.getValue(0,2) << endl;
	return 0;
}
