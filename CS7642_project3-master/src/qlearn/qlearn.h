#ifndef QLEARN_H
#define QLEARN_H

#include <iomanip>
#include <vector>
#include <sstream>
#include "../soccer/soccer_arena.h"

using namespace std;

namespace QLEARN
{
class QLearner{
	int n_states_;
	int n_actions_;
	public:
	float q_m_[SA::N_ACTIVE_STATES][SA::N_ACTIONS][SA::N_ACTIONS];
	QLearner(int n_states, int n_actions, float initialValue);
	void initializeQMatrix(float initialValue);
	stringstream getSummary();
	stringstream getLongSummary();
	float getValue(int state, int action_a, int action_b);
	void setValue(int state, int action_a, int action_b, float value);
	float EstimateValue(int state);

};
}

#endif /* QLEARN_H */
