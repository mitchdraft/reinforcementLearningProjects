#ifndef QLEARN_CC
#define QLEARN_CC

#include "qlearn.h"
#include <vector>

using namespace std;

namespace QLEARN
{
QLearner::QLearner(int n_states, int n_actions, float initialValue){
	n_states_ = n_states;
	n_actions_ = n_actions;
	initializeQMatrix(initialValue);
}
void QLearner::initializeQMatrix(float initialValue){
	for(int i = 0; i<n_states_; i++){
		for(int j = 0; j<n_actions_; j++){
			for(int k = 0; k<n_actions_; k++){
				q_m_[i][j][k] = initialValue;
			}
		}
	}
}
stringstream QLearner::getSummary(){
	stringstream ss;
	for(int i = 0; i<n_states_; i++){
		ss << i << "|";
		for(int j = 0; j<n_actions_; j++){
			for(int k = 0; k<n_actions_; k++){
				ss << q_m_[i][j][k];
				ss << ", ";
			}
		}
		ss << endl;
	}
	return ss;
}
stringstream QLearner::getLongSummary(){
	stringstream ss;
	ss << setprecision(2);
	ss << scientific;
	const string headers[n_states_] = {"N","S","E","W","X"};
	for(int i = 0; i<n_states_; i++){
		ss << "(" << i << ")" << endl;
		ss << "A\\B:  N,        S,        E,        W,        X" << endl;
		for(int j = 0; j<n_actions_; j++){
			ss << headers[j] << ": ";
			for(int k = 0; k<n_actions_; k++){
				ss << q_m_[i][j][k];
				ss << ", ";
			}
			ss << endl;
		}
	}
	return ss;
}
float QLearner::getValue(int state, int action_a, int action_b){
	return q_m_[state][action_a][action_b];
}
void QLearner::setValue(int state, int action_a, int action_b, float value){
	q_m_[state][action_a][action_b] = value;
}

float QLearner::EstimateValue(int state){
	float max = q_m_[state][0][0];
	for(int i = 0; i < n_actions_; i++){
		for(int j = 0; j < n_actions_; j++){
			if(q_m_[state][i][j] > max){
				max = q_m_[state][i][j];
			}
		}
	}
	return max;
}

} // namespace QLEARN

#endif /* QLEARN_CC */

