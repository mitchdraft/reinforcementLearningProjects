#ifndef SOLVER_CC
#define SOLVER_CC

#include "solver.h"

using namespace std;

namespace SOLVER
{

Solver::Solver(){
	Initialize();
}

void Solver::Initialize(){
	si_ = new OsiClpSolverInterface;
	clpSolver_ = dynamic_cast< OsiClpSolverInterface*> (si_);
	lpSolver_ = clpSolver_->getModelPtr();
	lpSolver_->setLogLevel(0);
	matrix_ = new CoinPackedMatrix(false,0,0);
	matrix_->setDimensions(0, n_cols_);
	SetObjectiveConstraints();
	SetPositivityConstraints();
	SetRowBounds();
	InitializeQMatrixConstraints();
}
void Solver::SetObjectiveConstraints(){
	// Update this according to the problem
	// this form seeks a maximum:
	for(int i = 0; i < N_JOINT_ACTIONS; i++){
		objective_[i] = 1.0;
	}
};

void Solver::SetPositivityConstraints(){
	for(int i = 0; i < N_POSITIVE_CONSTRAINTS; i++){
		col_lb_[i] = 0.0;
		col_ub_[i] = si_->getInfinity();
	}
}

void Solver::SetRowBounds(){
	// use the form: -inf < a x0 + b x1 + c x2 + ... <= 0
	for(int i = 0; i < N_ACTION_CONSTRAINTS; i++){
		row_lb_[i] = -1.0 * si_->getInfinity();
		row_ub_[i] = 0.0;
	}
	// Equality constraints
	// if we set the lower and upper bounds to the same value, we get equality
	double equality_value = 1.0;
	row_lb_[FIRST_EQUALITY_ROW_INDEX]  = equality_value;
	row_ub_[FIRST_EQUALITY_ROW_INDEX]  =  equality_value;
}
void Solver::InitializeQMatrixConstraints(){
	for(int i = 0; i < N_ACTION_CONSTRAINTS; i++){
		CoinPackedVector row;
		matrix_->appendRow(row);
	}

	// Add a row for the equality constraint:
	CoinPackedVector row;
	matrix_->appendRow(row);

	// Set row to all 1's for equality constraint:
	for(int i = 0; i < N_JOINT_ACTIONS; i++){
		matrix_->modifyCoefficient(FIRST_EQUALITY_ROW_INDEX, i, 1.0);
	}
}

void Solver::UpdateQMatrixConstraintsFromArrayAndSolve(LpArray lpa){
	// sweep through rows:
	for(int i = 0; i < SWEEP_MAP_LENGTH; i++){
		int anchor_index = sweep_map[i][0];
		int comparison_index = sweep_map[i][1];
		for(int j = 0; j < N_SOCCER_ACTIONS; j++){
			float delta_q = lpa.arr[comparison_index][j] - lpa.arr[anchor_index][j];
			matrix_->modifyCoefficient(i, anchor_index*N_SOCCER_ACTIONS + j, delta_q);
		}
	}
	// sweep through cols:
	for(int i = SWEEP_MAP_LENGTH; i < SWEEP_MAP_LENGTH * 2; i++){
		int anchor_index = sweep_map[i-SWEEP_MAP_LENGTH][0];
		int comparison_index = sweep_map[i-SWEEP_MAP_LENGTH][1];
		for(int j = 0; j < N_SOCCER_ACTIONS; j++){
			// this line is different from the above block:
			float delta_q = lpa.arr[j][comparison_index] - lpa.arr[j][anchor_index];
			// note that the column player has the negative utility of the row player:
			matrix_->modifyCoefficient(i, j*N_SOCCER_ACTIONS + anchor_index, -delta_q);
		}
	}
	SolveSystem();
}

void Solver::UpdateQMatrixConstraintsAndSolveCHICKEN(){

	matrix_->modifyCoefficient(0, 0, 1.0);
	matrix_->modifyCoefficient(0, 1, -2.0);
	matrix_->modifyCoefficient(1, 2, -1.0);
	matrix_->modifyCoefficient(1, 3, 2.0);
	matrix_->modifyCoefficient(2, 0, 1.0);
	matrix_->modifyCoefficient(2, 2, -2.0);
	matrix_->modifyCoefficient(3, 0, -1.0);
	matrix_->modifyCoefficient(3, 3, 2.0);

	SolveSystem();
}

void Solver::ChangeConstraintsArb(){
	matrix_->modifyCoefficient(0,0,-5.0);
	SolveSystem();
}
void Solver::SolveSystem(){
	si_->loadProblem(*matrix_, col_lb_, col_ub_, objective_, row_lb_, row_ub_);
	si_->initialSolve();
}
void Solver::WriteFile(){
	si_->writeMps("mktexample");
}
SolverResponse Solver::GetSolution(bool print_out){
	SolverResponse sr;
	sr.objective_value = si_->getObjValue();
	const double *solution = si_->getColSolution();
	for (int i = 0; i < n_cols_; i++){
		sr.prob[i/N_SOCCER_ACTIONS][i%N_SOCCER_ACTIONS] = solution[i];
		double val = solution[i];
		//std::cout << i << " " << val << std::endl;
	}
	if(print_out){
		std::cout << "Objective value is " << sr.objective_value << std::endl;
		for(int i = 0; i < N_SOCCER_ACTIONS; i++){
			for(int j = 0; j < N_SOCCER_ACTIONS; j++){
				cout << sr.prob[i][j] << ", ";
			}
			cout << endl;
		}
	}
	if(allow_warnings_){
		if ( !si_->isProvenOptimal() ) {
			cout << "WARNING: did not find optimal solution" << endl;
		}
	}
	return sr;
}

} //namespace SOLVER

#endif /* SOLVER_CC */
