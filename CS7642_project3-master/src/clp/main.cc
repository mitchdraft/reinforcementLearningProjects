#include "solver.cc"
#include <iostream>

using namespace std;

int main(){
	cout << "testing out the solver" << endl;
	SOLVER::Solver s = SOLVER::Solver();
	float arr[SOLVER::N_SOCCER_ACTIONS][SOLVER::N_SOCCER_ACTIONS] = {
		{.1,2,0,3,0},
		{1,-10,3.50,-10,40},
		{20,.20,0,0,.20},
		{0,0,.20,-30,.20},
		{0,0,7,.20,.20},
	};
	s.UpdateQMatrixConstraintsFromArrayAndSolve(arr);
	SOLVER::SolverResponse sr = s.GetSolution();

	float arr2[SOLVER::N_SOCCER_ACTIONS][SOLVER::N_SOCCER_ACTIONS] = {
		{6,2,-12,0,0},
		{-7,0,0,0,0},
		{2,0,0,0,0},
		{0,0,0,0,0},
		{0,0,0,0,0},
	};
	s.UpdateQMatrixConstraintsFromArrayAndSolve(arr2);
	sr = s.GetSolution();

	float arr3[SOLVER::N_SOCCER_ACTIONS][SOLVER::N_SOCCER_ACTIONS] = {
		{1,2,-3,0,0},
		{2,3,0,0,0},
		{-3,0,-10,0,0},
		{0,0,0,0,0},
		{0,0,0,0,0},
	};
	s.UpdateQMatrixConstraintsFromArrayAndSolve(arr3);
	sr = s.GetSolution();

	float arr4[SOLVER::N_SOCCER_ACTIONS][SOLVER::N_SOCCER_ACTIONS] = {
		{0,0,0,0,0},
		{0,0,0,0,0},
		{0,0,0,0,0},
		{0,0,0,0,0},
		{0,0,0,0,0},
	};
	s.UpdateQMatrixConstraintsFromArrayAndSolve(arr4);
	sr = s.GetSolution();

	float arr5[SOLVER::N_SOCCER_ACTIONS][SOLVER::N_SOCCER_ACTIONS] = {
		{0,-2,0,0,0},
		{2,0,0,0,0},
		{0,0,0,0,0},
		{0,0,0,0,0},
		{0,0,0,0,0},
	};
	s.UpdateQMatrixConstraintsFromArrayAndSolve(arr5);
	sr = s.GetSolution();
	s.WriteFile();

	float arr6[SOLVER::N_SOCCER_ACTIONS][SOLVER::N_SOCCER_ACTIONS] = {
		{.1,2,0,0,0},
		{-12,1,-10,0,0},
		{10,0,0,0,0},
		{.10,0,.5,0,.5},
		{0,0,0,.5,.5},
	};
	s.UpdateQMatrixConstraintsFromArrayAndSolve(arr6);
	sr = s.GetSolution();

	return 0;
}
