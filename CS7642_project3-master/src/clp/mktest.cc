// Example of using COIN-OR OSI, building the instance internally
// with sparse matrix object
#include <iostream>
#include "OsiClpSolverInterface.hpp"
#include "CoinPackedMatrix.hpp"
#include "CoinPackedVector.hpp"

const int N_JOINT_ACTIONS = 25;
struct SolverResponse {
	float objective_value;
	float prob[N_JOINT_ACTIONS]; // array of probabilities
};

int SampleFunction(void) {
	//// Create a problem pointer. We use the base class here.
	//OsiSolverInterface *si;
	//// When we instantiate the object, we need a specific derived class.
	//si = new OsiClpSolverInterface;
	////ClpSimplex * clps = si->getModelPtr();
	

	// use this variant to supress output
	// (funky call prep borrowed from: https://projects.coin-or.org/Cbc/browser/stable/2.9/Cbc/src/CbcSolver.cpp?rev=2209)
	OsiSolverInterface * si;
	si = new OsiClpSolverInterface;
	OsiClpSolverInterface * clpSolver = dynamic_cast< OsiClpSolverInterface*> (si);
	ClpSimplex * lpSolver = clpSolver->getModelPtr();
	lpSolver->setLogLevel(0);

	//ClpSimplex * clps = si->modelPtr_;
	//std::out << si->specialOptions() << std::endl;
	// Build our own instance from scratch
	/*
	 * This section adapted from Matt Galati’s example
	 * on the COIN-OR Tutorial website.
	 *
	 * Problem from Bertsimas, Tsitsiklis page 21
	 *
	 * optimal solution: x* = (1,1)
	 *
	 * minimize -1 x0 - 1 x1
	 * s.t 1 x0 + 2 x1 <= 3
	 * 2 x0 + 1 x1 <= 3
	 * x0 >= 0
	 * x1 >= 0
	 *
	 * chicken:
	 * minimize:
	 * -1 x0 -1 x1 -1 x2 -1 x3
	 * s.t:
	 *  1 x0 - 2 x1               <= 0
	 *  0 x0 + 0 x1 - 1 x2 + 2 x3 <= 0
	 *  1 x0 + 0 x1 - 2 x2 + 0 x3 <= 0
	 * -1 x0 + 0 x1 + 0 x2 + 2 x3 <= 0
	 *  
	 *  1 x0 + 1 x1 + 1 x2 + 1 x3 <= 1
	 * -1 x0 - 1 x1 - 1 x2 - 1 x3 <=-1
	 *
	 */
	int n_cols = 4;
	double * objective = new double[n_cols];//the objective coefficients
	double * col_lb = new double[n_cols];//the column lower bounds
	double * col_ub = new double[n_cols];//the column upper bounds
	//Define the objective coefficients.
	//minimize -1 x0 - 1 x1
	// -1 x0 -1 x1 -1 x2 -1 x3
	objective[0] = 1.0;
	objective[1] = 1.0;
	objective[2] = 1.0;
	objective[3] = 1.0;
	//Define the variable lower/upper bounds.
	// x0 >= 0 => 0 <= x0 <= infinity
	// x1 >= 0 => 0 <= x1 <= infinity
	col_lb[0] = 0.0;
	col_lb[1] = 0.0;
	col_lb[2] = 0.0;
	col_lb[3] = 0.0;
	col_ub[0] = si->getInfinity();
	col_ub[1] = si->getInfinity();
	col_ub[2] = si->getInfinity();
	col_ub[3] = si->getInfinity();
	int n_rows = 6;
	double * row_lb = new double[n_rows]; //the row lower bounds
	double * row_ub = new double[n_rows]; //the row upper bounds
	//Define the constraint matrix.
	CoinPackedMatrix * matrix = new CoinPackedMatrix(false,0,0);
	matrix->setDimensions(0, n_cols);
	//1 x0 + 2 x1 <= 3 => -infinity <= 1 x0 + 2 x2 <= 3
	CoinPackedVector row0;
	row0.insert(0, 1.0);
	row0.insert(1, -2.0);
	row_lb[0] = -1.0 * si->getInfinity();
	row_ub[0] = 0.0;
	matrix->appendRow(row0);
	//2 x0 + 1 x1 <= 3 => -infinity <= 2 x0 + 1 x1 <= 3
	CoinPackedVector row1;
	row1.insert(2, -1.0);
	row1.insert(3, 2.0);
	row_lb[1] = -1.0 * si->getInfinity();
	row_ub[1] = 0.0;
	matrix->appendRow(row1);

	CoinPackedVector row2;
	row2.insert(0, 1.0);
	row2.insert(2, -2.0);
	row_lb[2] = -1.0 * si->getInfinity();
	row_ub[2] = 0.0;
	matrix->appendRow(row2);

	CoinPackedVector row3;
	row3.insert(0, -1.0);
	row3.insert(3, 2.0);
	row_lb[3] = -1.0 * si->getInfinity();
	row_ub[3] = 0.0;
	matrix->appendRow(row3);

	CoinPackedVector row4;
	row4.insert(0, 1.0);
	row4.insert(1, 1.0);
	row4.insert(2, 1.0);
	row4.insert(3, 1.0);
	row_lb[4] = -1.0 * si->getInfinity();
	row_ub[4] = 1.0;
	matrix->appendRow(row4);

	CoinPackedVector row5;
	row5.insert(0, -1.0);
	row5.insert(1, -1.0);
	row5.insert(2, -1.0);
	row5.insert(3, -1.0);
	row_lb[5] = -1.0 * si->getInfinity();
	row_ub[5] = -1.0;
	matrix->appendRow(row5);

	//load the problem to OSI
	si->loadProblem(*matrix, col_lb, col_ub, objective, row_lb, row_ub);
	//write the MPS file to a file called example.mps
	//si->writeMps("mktexample");
	// Solve the (relaxation of the) problem
	si->initialSolve();
	// Check the solution
	SolverResponse sr;
	sr.objective_value = si->getObjValue();
	if ( si->isProvenOptimal() )
	{
		//std::cout << "Found optimal solution!" << std::endl;
		std::cout << "Objective value is " << sr.objective_value << std::endl;
		int n = si->getNumCols();
		const double *solution;
		solution = si->getColSolution();
		for (int i = 0; i < n_cols; i++){
			sr.prob[i] = solution[i];
			double val = solution[i];
			std::cout << i << " " << val << std::endl;
		}
		//double * columnPrimal = si->primalColumnSolution(); // not avaliable
		//std::cout << solution;
		// We could then print the solution or examine it.
	}
	else
	{
		std::cout << "Didn’t find optimal solution." << std::endl;
		// Could then check other status functions.
	}
	return 0;
}

int main(){
	SampleFunction();
	return 0;
}
