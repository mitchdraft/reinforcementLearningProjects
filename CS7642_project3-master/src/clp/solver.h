#ifndef SOLVER_H
#define SOLVER_H
#include <iostream>
#include "OsiClpSolverInterface.hpp"
#include "CoinPackedMatrix.hpp"
#include "CoinPackedVector.hpp"

using namespace std;

namespace SOLVER
{

const int SWEEP_MAP_LENGTH = 20;
const int sweep_map[][2] = {
	//{0,0},
	{0,1},
	{0,2},
	{0,3},
	{0,4},
	{1,0},
	//{1,1},
	{1,2},
	{1,3},
	{1,4},
	{2,0},
	{2,1},
	//{2,2},
	{2,3},
	{2,4},
	{3,0},
	{3,1},
	{3,2},
	//{3,3},
	{3,4},
	{4,0},
	{4,1},
	{4,2},
	{4,3},
	//{4,4},
};
const int N_SOCCER_ACTIONS = 5;
const int N_JOINT_ACTIONS = 25;
//const int N_JOINT_ACTIONS = 4; // Chicken
const int N_POSITIVE_CONSTRAINTS = N_JOINT_ACTIONS;
const int N_ACTION_CONSTRAINTS = 40;
//const int N_ACTION_CONSTRAINTS = 4; // Chicken
const int N_EQUALITY_CONSTRAINTS = 1;
const int FIRST_EQUALITY_ROW_INDEX = N_ACTION_CONSTRAINTS + 0;
struct SolverResponse {
	float objective_value;
	float prob[N_SOCCER_ACTIONS][N_SOCCER_ACTIONS]; // matrix of probabilities
};
struct LpArray {
	float arr[N_SOCCER_ACTIONS][N_SOCCER_ACTIONS];
};

class Solver {
	int n_cols_ = N_JOINT_ACTIONS;
	int n_rows_ = N_JOINT_ACTIONS + N_POSITIVE_CONSTRAINTS + N_EQUALITY_CONSTRAINTS;
	double * objective_ = new double[n_cols_];//the objective coefficients
	double * col_lb_ = new double[n_cols_];//the column lower bounds
	double * col_ub_ = new double[n_cols_];//the column upper bounds
	double * row_lb_ = new double[n_rows_]; //the row lower bounds
	double * row_ub_ = new double[n_rows_]; //the row upper bounds
	OsiSolverInterface * si_;
	OsiClpSolverInterface * clpSolver_;
	ClpSimplex * lpSolver_;
	CoinPackedMatrix * matrix_;
	bool allow_warnings_ = false;
	public:
	Solver();
	void Initialize();
	void SetPositivityConstraints();
	void SetRowBounds();
	void SetObjectiveConstraints();
	void InitializeQMatrixConstraints();
	void UpdateQMatrixConstraintsFromArrayAndSolve(LpArray lpa);
	void UpdateQMatrixConstraintsAndSolveCHICKEN();
	void ChangeConstraintsArb();
	void SolveSystem();
	void WriteFile();
	SolverResponse GetSolution(bool print_out = false);
};

} // namespace SOLVER
#endif /* SOLVER_H */
