#include <limits.h>
#include "../soccer/soccer_arena.h"
#include "gtest/gtest.h"
namespace {

TEST(ArenaTest, First) {
	SA::SoccerArena a1 = SA::SoccerArena("test",1,2,true,false);
	EXPECT_EQ(a1.getName(), "test");
	EXPECT_EQ(a1.encodeState(false,1,2), 6);
	SA::Transition transition_0 = a1.GetTransition(0);
	// when b goes east from position 1, it will score
	EXPECT_EQ(transition_0.bFirstReward[SA::NORTH][SA::EAST], -100);
}
//TEST(ArenaTest, Second) {
//	SA::SoccerArena a1 = SA::SoccerArena("test",1,2,true,false);
//
//	a1.SetupByComponentIndex(0);
//	EXPECT_EQ(a1.GetState(), 0);
//	int move_reward = a1.move(false,SA::EAST);
//	//EXPECT_EQ(a1.GetState(),-1);
//	a1.SetupByComponentIndex(1);
//	EXPECT_EQ(a1.GetState(), 1);
//}

}  // namespace
