#ifndef AGENT_H
#define AGENT_H

#include <vector>
#include <sstream>
#include <limits>
#include <algorithm>
#include <random>
#include <unordered_set>
#include "qlearn/qlearn.cc"
#include "../clp/solver.h"

using namespace std;

namespace AGENT
{

struct ActionChoice {
	int player_a_action;
	int player_b_action;
	float a_first_reward;
	float b_first_reward;
	int a_first_next_state;
	int b_first_next_state;
	int anticipated_value;
};

const int A_CORRELATEDQ = 2;
const int A_FOE = 2;
const int A_FRIEND = 2;
const int A_QLEARNER = 2;
const int NUMBER_OF_PLAYERS = 2;
const int ACTION_VECTOR_LENGTH = 5;
const float PI_A_FIRST = 0.5;
const float PI_B_FIRST = 1 - 0.5;
const int PLAYER_A_QM_INDEX = 0;
const int PLAYER_B_QM_INDEX = 1;

std::uniform_int_distribution<int> distribution_(0,4);
class Agent{
	int type_;
	int last_starting_state_;
	float epsilon_;
	float random_threshold_;
	unordered_set<int> a_choice_options_;
	unordered_set<int> b_choice_options_;
	std::default_random_engine generator_;
	int dice_roll_;
	public:
	QLEARN::QLearner *q_learner_;
	SOLVER::Solver solver_;
	Agent(int type, QLEARN::QLearner *ql, float epsilon);
	void initializeAgent();
	float GetCeqValue(int next_a_state, int next_b_state);
	float GetFriendValue(int next_a_state, int next_b_state);
	float GetRegularQValue(int next_a_state, int next_b_state);
	void ChooseRandomAction(ActionChoice *ac);
	float GetRandomFloat();
	int PickRowFromDistributionMatrix(float mat[ACTION_VECTOR_LENGTH][ACTION_VECTOR_LENGTH]);
	int PickColFromDistributionMatrix(float mat[ACTION_VECTOR_LENGTH][ACTION_VECTOR_LENGTH]);
	SOLVER::LpArray GenerateLPArray(SA::Transition tr);
	ActionChoice ChooseCeqAction(SA::Transition tr);
	ActionChoice chooseFriendAction(SA::Transition tr);
	ActionChoice chooseRegularQAction(SA::Transition tr, int current_state);
	float GetMaxRegularQValue(int state, int player_index);
	int GetStartingState();
};
}

#endif /* AGENT_H */
