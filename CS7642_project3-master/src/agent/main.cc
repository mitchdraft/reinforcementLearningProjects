#include <string>
#include <iostream>
#include "agent.cc"
#include "../qlearn/qlearn.cc"

int main(){
	QLEARN::QLearner ql = QLEARN::QLearner(24,5,1.0);
	cout << "Running agent sample app" << endl;
	AGENT::Agent ag = AGENT::Agent(AGENT::A_FRIEND, &ql);
	float optionsMatrix[AGENT::ACTION_VECTOR_LENGTH][AGENT::ACTION_VECTOR_LENGTH] = {
		{1.0,2.0,1,1,1},
		{1.0,2.0,1,1,1},
		{1.0,2.0,1,1,1},
		{10.0,99.0,1,1,1},
		{3.0,4,0,1,1}
	};
	int* ac = ag.chooseFriendAction(optionsMatrix);
	cout << "action choice: " << ac[0] << ", " << ac[1] << endl;
	return 0;
}


