#ifndef AGENT_CC
#define AGENT_CC

#include "agent.h"

using namespace std;

namespace AGENT
{
Agent::Agent(int type, QLEARN::QLearner *ql, float epsilon){
	type_ = type;
	q_learner_ = ql;
	solver_ = SOLVER::Solver();
	epsilon_ = epsilon;
	random_threshold_ = RAND_MAX * epsilon;
	initializeAgent();
}
void Agent::initializeAgent(){
	cout << "Agent initialized" << endl;
}

float Agent::GetCeqValue(int next_a_state, int next_b_state){
	return PI_A_FIRST * q_learner_->EstimateValue(next_a_state)
		+ PI_B_FIRST * q_learner_->EstimateValue(next_b_state);
}

float Agent::GetFriendValue(int next_a_state, int next_b_state){
	return PI_A_FIRST * q_learner_->EstimateValue(next_a_state)
		+ PI_B_FIRST * -q_learner_->EstimateValue(next_b_state); // opportunity: cache the result so you only have to do this once per unique next state
}

float Agent::GetRegularQValue(int next_a_state, int next_b_state){
	return PI_A_FIRST * q_learner_->EstimateValue(next_a_state)
		+ PI_B_FIRST * -q_learner_->EstimateValue(next_b_state);
}

void Agent::ChooseRandomAction(ActionChoice *ac){
	ac->player_a_action = distribution_(generator_);
	ac->player_b_action = distribution_(generator_);
}

float Agent::GetRandomFloat(){
	int limit = RAND_MAX;
	int val = rand();
	return (float)val/(float)limit;
}

int Agent::PickRowFromDistributionMatrix(float mat[ACTION_VECTOR_LENGTH][ACTION_VECTOR_LENGTH]){
	float target = GetRandomFloat();
	float sum = 0;
	int row = 0;
	for(int i = 0; i<ACTION_VECTOR_LENGTH; i++){
		for(int j = 0; j<ACTION_VECTOR_LENGTH; j++){
			sum+= mat[i][j];
			if(sum >= target)
				return row;
		}
		row++;
	}
	return 0;
}

int Agent::PickColFromDistributionMatrix(float mat[ACTION_VECTOR_LENGTH][ACTION_VECTOR_LENGTH]){
	float target = GetRandomFloat();
	float sum = 0;
	int row = 0;
	for(int i = 0; i<ACTION_VECTOR_LENGTH; i++){
		for(int j = 0; j<ACTION_VECTOR_LENGTH; j++){
			sum+= mat[j][i];
			if(sum >= target)
				return row;
		}
		row++;
	}
	return 0;
}

SOLVER::LpArray Agent::GenerateLPArray(SA::Transition tr){
	SOLVER::LpArray lpa;
	float arr[ACTION_VECTOR_LENGTH][ACTION_VECTOR_LENGTH];
	for(int i = 0; i < ACTION_VECTOR_LENGTH; i++){
		for(int j = 0; j < ACTION_VECTOR_LENGTH; j++){
			int next_a_state = tr.a_first_next_state[i][j];
			int next_b_state = tr.b_first_next_state[i][j];
			lpa.arr[i][j] = GetCeqValue(next_a_state, next_b_state);
		}
	}
	return lpa;
}

ActionChoice Agent::ChooseCeqAction(SA::Transition tr){
	ActionChoice ac;
	if(rand() > random_threshold_){
		// let's define the estimated value of the next state as the max value, across all possible actions, of the next state
		float max_value = -numeric_limits<float>::max();

		a_choice_options_.clear();
		b_choice_options_.clear();

		// generate input to LP
		SOLVER::LpArray lpa = GenerateLPArray(tr);
		// call LP
		solver_.UpdateQMatrixConstraintsFromArrayAndSolve(lpa);
		SOLVER::SolverResponse sr = solver_.GetSolution();
		// get actions from distribution
		ac.player_a_action = PickRowFromDistributionMatrix(sr.prob);
		ac.player_b_action = PickColFromDistributionMatrix(sr.prob);

	}
	else { //choose an action at random
		ChooseRandomAction( &ac);
		int next_a_state = tr.a_first_next_state[ac.player_a_action][ac.player_b_action];
		int next_b_state = tr.b_first_next_state[ac.player_a_action][ac.player_b_action];
		ac.anticipated_value = GetFriendValue(next_a_state, next_b_state); // note, that the concept of an agent's "anticipated_value" does not really hold when we chose the action at random - DISCUSS
	}
	ac.a_first_reward = tr.a_first_reward[ac.player_a_action][ac.player_b_action];
	ac.b_first_reward = tr.b_first_reward[ac.player_a_action][ac.player_b_action];
	ac.a_first_next_state = tr.a_first_next_state[ac.player_a_action][ac.player_b_action];
	ac.b_first_next_state = tr.b_first_next_state[ac.player_a_action][ac.player_b_action];
	return ac;
}

ActionChoice Agent::chooseFriendAction(SA::Transition tr){
	ActionChoice ac;
	if(rand() > random_threshold_){
		// let's define the estimated value of the next state as the max value, across all possible actions, of the next state
		float max_value = -numeric_limits<float>::max();

		a_choice_options_.clear();
		b_choice_options_.clear();

		for(int i = 0; i < ACTION_VECTOR_LENGTH; i++){
			for(int j = 0; j < ACTION_VECTOR_LENGTH; j++){
				int next_a_state = tr.a_first_next_state[i][j];
				int next_b_state = tr.b_first_next_state[i][j];
				if(next_a_state < SA::N_ACTIVE_STATES && next_b_state < SA::N_ACTIVE_STATES){
					float candidate_value = GetFriendValue(next_a_state, next_b_state);
					//cout << candidate_value << "|";
					if(candidate_value >= max_value){
						max_value = candidate_value;
						a_choice_options_.insert(i);
						b_choice_options_.insert(j);
					}
				}
			}
		}
		ac.player_a_action = -1;
		ac.player_b_action = -1;
		while(ac.player_a_action < 0){
			dice_roll_ = distribution_(generator_);
			if(a_choice_options_.find(dice_roll_) != a_choice_options_.end()){
				ac.player_a_action = dice_roll_;
			}
		}
		while(ac.player_b_action < 0){
			dice_roll_ = distribution_(generator_);
			if(b_choice_options_.find(dice_roll_) != b_choice_options_.end()){
				ac.player_b_action = dice_roll_;
			}
		}
		ac.anticipated_value = max_value;
	}
	else { //choose an action at random
		ChooseRandomAction( &ac);
		int next_a_state = tr.a_first_next_state[ac.player_a_action][ac.player_b_action];
		int next_b_state = tr.b_first_next_state[ac.player_a_action][ac.player_b_action];
		ac.anticipated_value = GetFriendValue(next_a_state, next_b_state); // note, that the concept of an agent's "anticipated_value" does not really hold when we chose the action at random - DISCUSS
	}
	ac.a_first_reward = tr.a_first_reward[ac.player_a_action][ac.player_b_action];
	ac.b_first_reward = tr.b_first_reward[ac.player_a_action][ac.player_b_action];
	ac.a_first_next_state = tr.a_first_next_state[ac.player_a_action][ac.player_b_action];
	ac.b_first_next_state = tr.b_first_next_state[ac.player_a_action][ac.player_b_action];
	return ac;
}
// A choose action that has been best for it in the past (first row of Q)
// B choose action that has been best for it in the past (second row of Q)
ActionChoice Agent::chooseRegularQAction(SA::Transition tr, int current_state){
	ActionChoice ac;
	if(rand() > random_threshold_){
		// let's define the estimated value of the next state as the max value, across all possible actions, of the next state
		float max_value_a = -numeric_limits<float>::max();
		float max_value_b = -numeric_limits<float>::max();

		a_choice_options_.clear();
		b_choice_options_.clear();

		for(int i = 0; i < ACTION_VECTOR_LENGTH; i++){
			if(q_learner_->q_m_[current_state][PLAYER_A_QM_INDEX][i] > max_value_a){
				max_value_a = q_learner_->q_m_[current_state][PLAYER_A_QM_INDEX][i];
				a_choice_options_.insert(i);
			}
			if(q_learner_->q_m_[current_state][PLAYER_B_QM_INDEX][i] > max_value_b){
				max_value_b = q_learner_->q_m_[current_state][PLAYER_B_QM_INDEX][i];
				b_choice_options_.insert(i);
			}
		}
		ac.player_a_action = -1;
		ac.player_b_action = -1;
		while(ac.player_a_action < 0){
			dice_roll_ = distribution_(generator_);
			if(a_choice_options_.find(dice_roll_) != a_choice_options_.end()){
				ac.player_a_action = dice_roll_;
			}
		}
		while(ac.player_b_action < 0){
			dice_roll_ = distribution_(generator_);
			if(b_choice_options_.find(dice_roll_) != b_choice_options_.end()){
				ac.player_b_action = dice_roll_;
			}
		}
		ac.anticipated_value = max_value_a; // NOTE - this breaks the mold a bit. We know that A and B have opposite and equal priorities, which means we can sort of use the same q matrix. However, this is the issue with that simplification: A and B will have different exectations of the value (it may not matter though)
	}
	else { //choose an action at random
		ChooseRandomAction( &ac);
		ac.anticipated_value = q_learner_->q_m_[current_state][PLAYER_A_QM_INDEX][ac.player_a_action];
	}
	ac.a_first_reward = tr.a_first_reward[ac.player_a_action][ac.player_b_action];
	ac.b_first_reward = tr.b_first_reward[ac.player_a_action][ac.player_b_action];
	ac.a_first_next_state = tr.a_first_next_state[ac.player_a_action][ac.player_b_action];
	ac.b_first_next_state = tr.b_first_next_state[ac.player_a_action][ac.player_b_action];
	return ac;
}
float Agent::GetMaxRegularQValue(int state, int player_index){
	float max_value = -numeric_limits<float>::max();
	for(int i = 0; i < ACTION_VECTOR_LENGTH; i++){
		if(q_learner_->q_m_[state][player_index][i] > max_value){
			max_value = q_learner_->q_m_[state][player_index][i];
		}
	}
	return max_value;
}

int Agent::GetStartingState(){
	last_starting_state_++;
	last_starting_state_%=SA::N_ACTIVE_STATES;
	return last_starting_state_;
}



} // namespace

#endif /* AGENT_CC */
