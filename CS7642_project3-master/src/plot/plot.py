import matplotlib.pyplot as plt
import time

#file_name = '../apps/ceq/tmp_log0.tmp';
#file_name = '../apps/friend/tmp_log1.tmp';
file_name = 'tmp_log0.tmp';

#file_name = '../apps/regularq/tmp_log0.tmp';
with open(file_name) as f:
  read_data = f.read().splitlines()

#plt.plot([1,2,3,4])
fig, ax = plt.subplots();
ax.plot(read_data)
#ax.plot(read_data, '.')
#ax.set_ybound(0,0.5);
#ax.set_xbound(0,10000000);
plt.ylabel('Q-value difference')
plt.show()
