#ifndef SOCCER_CONSTANTS_H
#define SOCCER_CONSTANTS_H

namespace SA
{
const int N_ACTIONS = 5;
const int N_JOINT_ACTIONS = N_ACTIONS*N_ACTIONS;

const int action_list[] = {0,1,2,3,4};
const int NORTH = 0;
const int SOUTH = 1;
const int EAST  = 2;
const int WEST  = 3;
const int STICK = 4;

const int N_ACTIVE_STATES = 24;
const int N_TERMINAL_STATES = 2;
const int N_STATES = N_ACTIVE_STATES + N_TERMINAL_STATES;
const int TERMINAL_A_INDEX = 24;
const int TERMINAL_B_INDEX = 25;
const int TERMINAL_A_VALUE = 24000;
const int TERMINAL_B_VALUE = 25000;
} // namespace


#endif /* SOCCER_CONSTANTS_H */
