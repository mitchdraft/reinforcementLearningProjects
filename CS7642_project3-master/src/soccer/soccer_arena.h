#ifndef SOCCER_ARENA_H
#define SOCCER_ARENA_H

#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <unordered_map>
#include "constants.h"

using namespace std;

namespace SA
{

struct Transition {
	int a_first_next_state[N_ACTIONS][N_ACTIONS];
	int b_first_next_state[N_ACTIONS][N_ACTIONS];
	float a_first_reward[N_ACTIONS][N_ACTIONS];
	float b_first_reward[N_ACTIONS][N_ACTIONS];
};

// a 2x2 soccer grid:
// [A][ ][ ][B]
// [A][ ][ ][B]
//
// grid positions identified by number:
// [0][1][2][5]
// [0][3][4][5]
//
// configuration represented in figure 4
// [A][ B*][ A ][B]
// [A][   ][   ][B]

// Transition matrix for [gridPosition][action] pairs
static const int actionTransitions[6][N_ACTIONS] = {
	{-1, -1, -1, -1, -1}, // Transitions from actions taken in state 0 - these are meaningless padding, state 0 is terminal
	{ 1,  3,  2,  0,  1}, // Transitions from actions taken in state 1
	{ 2,  4,  5,  1,  2}, // " ''' " 2
	{ 1,  3,  4,  0,  3}, // " ''' " 3
	{ 2,  4,  5,  3,  4}, // " ''' " 4
	{-1, -1, -1, -1, -1}, // " ''' " 5 - these are meaningless, state 5 is terminal
};

static const int states_by_components[][3] { // 24 states composed of: [a_has_ball][pos_a][pos_b]
	{0,1,2}, // 0
	{0,1,3},
	{0,1,4},
	{0,2,1}, // 3 (Figure 4)
	{0,2,3},
	{0,2,4}, // 5
	{0,3,1},
	{0,3,2},
	{0,3,4},
	{0,4,1},
	{0,4,2}, // 10
	{0,4,3},
	{1,1,2},
	{1,1,3},
	{1,1,4},
	{1,2,1}, // 15
	{1,2,3},
	{1,2,4},
	{1,3,1},
	{1,3,2},
	{1,3,4}, // 20
	{1,4,1},
	{1,4,2},
	{1,4,3}, // 23
};

class SoccerArena {
	string name_;
	bool debug_ = true;

	int goal_a_ = 0;
	int goal_b_ = 5;
	float goal_reward_ = 100;

	// Rewards from the perspective of player A:
//	static vector<vector<int> > action_rewards_a = {
//		{ 0, 0, 0,            0,           0}, // Rewards from actions taken in state 0 - these are meaningless padding, state 0 is terminal
//		{ 0, 0, 0,            goal_reward, 0}, // Rewards from actions taken in state 1
//		{ 0, 0, -goal_reward, 0,           0}, // " ''' " 2
//		{ 0, 0, 0,            goal_reward, 0}, // " ''' " 3
//		{ 0, 0, -goal_reward, 0,           0}, // " ''' " 4
//		{ 0, 0, 0,            0,           0}, // " ''' " 5 - these are meaningless, state 5 is terminal
//	};
	// Rewards from the perspective of player B are the negative of those for player A

	// have components, want component id:
	// -get hash
	// -componentId = stateMap(hash)
	bool a_has_ball_;
	int pos_a_;
	int pos_b_;
	bool game_over_;
	int game_count_ = 0;
	unordered_map<int,int> stateMap_; // maps from hash to state tuple index
	unordered_map<int,Transition> stateTransitionMap_;
	int state_;

	public:
	SoccerArena(string nname, int pa, int pb, bool initial_a_has_ball, bool use_debug);
	string getName(){
		return name_;
	}

	// for debugging
	int stateHashList_[N_STATES];


	void initStateMap();
	void initStateTransitionMap();
	Transition GetTransition(int state_id);
	stringstream summarizeStateTransitionMap(int stmID);
	stringstream DumpState();

	float move(bool move_a, int action);

	int moveAB(bool move_a_first, int act_a, int act_b);

	int Reset();
	int GetInnerState();

	int GetState();
	void SetupByStateID(int stateID);
	void SetupByComponentIndex(int componentIndex);
	//int encodeState(const int ar[3]);
	int encodeState();
	int encodeState(bool a_has_ball_yn, const int ap, const int bp);
	string reciteStates();

	string Display();

	int getGameCount(){
		return game_count_;
	}
};

}; // end namespace

#endif /* SOCCER_ARENA_H */


