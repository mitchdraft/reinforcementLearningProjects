#ifndef SOCCER_ARENA_CC
#define SOCCER_ARENA_CC

#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include "soccer_arena.h"

using namespace std;

namespace SA
{
	SoccerArena::SoccerArena(string nname, int pa, int pb, bool initial_a_has_ball, bool use_debug){
		debug_ = use_debug;
		initStateMap();
		initStateTransitionMap();
		name_ = nname;
		a_has_ball_ = initial_a_has_ball;
		pos_a_ = pa;
		pos_b_ = pb;
		game_over_ = false;
	}


	void SoccerArena::initStateMap(){
		for(int i = 0; i < N_ACTIVE_STATES; i++){
			int stateHash = encodeState(
				(states_by_components[i][0] == 1) ? true : false,
				states_by_components[i][1],
				states_by_components[i][2]
			);
			//cout << key << endl;
			stateHashList_[i] = stateHash;
			stateMap_[stateHash] = i;
		}
		stateHashList_[TERMINAL_A_INDEX] = TERMINAL_A_INDEX;
		stateHashList_[TERMINAL_B_INDEX] = TERMINAL_B_INDEX;
		stateMap_[TERMINAL_A_VALUE] = TERMINAL_A_INDEX;
		stateMap_[TERMINAL_B_VALUE] = TERMINAL_B_INDEX;
	}
	// crawl along all the possible states, transitions, and transition sequences in order to collect the next states and rewards for when either player A or B take the action
	void SoccerArena::initStateTransitionMap(){
		for(int state_id = 0; state_id < N_ACTIVE_STATES; state_id++){
			Transition transition;
			for(int action_a = 0; action_a < N_ACTIONS; action_a++){
				for(int action_b = 0; action_b < N_ACTIONS; action_b++){
					SetupByComponentIndex(state_id);
					int reward_a = moveAB(true, action_a, action_b);
					transition.a_first_reward[action_a][action_b] = reward_a;
					transition.a_first_next_state[action_a][action_b] = state_;

					SetupByComponentIndex(state_id);
					reward_a = moveAB(false, action_a, action_b);
					transition.b_first_reward[action_a][action_b] = reward_a;
					transition.b_first_next_state[action_a][action_b] = state_;

					if(debug_)
						cout << transition.b_first_reward[action_a][action_b] << ",";
				}
			}
			pair<int, Transition> transPair(state_id, transition);
			stateTransitionMap_.insert(transPair);
			if(debug_)
				cout << " (" << state_id << endl;
			SetupByComponentIndex(state_id);
			if(debug_)
				cout << Display();
		}
	}

	Transition SoccerArena::GetTransition(int state_id){
		return stateTransitionMap_[state_id];
	}
	int SoccerArena::GetInnerState(){
		return state_;
	}
	
	//for testing
	stringstream SoccerArena::summarizeStateTransitionMap(int state_id){
		stringstream ss, s1,s2,s3,s4;
		s1 << "A first Reward: ";
		s2 << "B first Reward: ";
		s3 << "A first NextSt: ";
		s4 << "B first NextSt: ";
		Transition tr = stateTransitionMap_[state_id];
		for(int j = 0; j < N_ACTIONS; j++){
			for(int k = 0; k < N_ACTIONS; k++){
				s1 << tr.a_first_reward[j][k] << ",";
				s2 << tr.b_first_reward[j][k] << ",";
				s3 << tr.a_first_next_state[j][k] << ",";
				s4 << tr.b_first_next_state[j][k] << ",";
			//ss << endl;
			}
			s1 << " | ";
			s2 << " | ";
			s3 << " | ";
			s4 << " | ";
		}
		ss << s1.str() << endl << s2.str() << endl << s3.str() << endl << s4.str() << endl;
		return ss;
	}
	stringstream SoccerArena::DumpState(){
		stringstream ss;
		ss << "a has ball? " << (a_has_ball_ ? "yes" : "no") << endl;
		ss << "pos_a? " << pos_a_ << "; ";
		ss << "pos_b? " << pos_b_ << "; ";
		//ss << "(state id, state id's hash, state id)" << endl;
		//for(int i = 0; i<N_STATES; i++){
		//	ss << "(" << i << "," << stateHashList_[i] << "," << stateMap_[stateHashList_[i]] << ") ";
		//}
		return ss;
	}

	

	// for testing
	string SoccerArena::reciteStates(){
		string st = "";
		for(int i = 0; i < sizeof(states_by_components)/sizeof(states_by_components[0]); i++){
			st += to_string(states_by_components[i][0]) + ", ";
			st += to_string(states_by_components[i][1]) + ", ";
			st += to_string(states_by_components[i][2]) + "\n";
		}
		return st;
	}

	int SoccerArena::encodeState(){
		return encodeState(a_has_ball_, pos_a_, pos_b_);
	}
	int SoccerArena::encodeState(bool a_has_ball_yn, const int ap, const int bp){
			int a_has_b = a_has_ball_yn
				? 1
				: 0;
			int stateHash = 16 * a_has_b;
			stateHash += 4 * ap;
			stateHash += bp;
			return stateHash;
	}

	float SoccerArena::move(bool move_a, int action){

		// initialize parameters as if B has the ball
		int opponent_pos = pos_a_;
		int current_pos = pos_b_;
		int target_goal = goal_b_;
		int opponent_target_goal = goal_a_;

		if(move_a){
			opponent_pos = pos_b_;
			current_pos = pos_a_;
			target_goal = goal_a_;
			opponent_target_goal = goal_b_;
		}

		int target_pos = actionTransitions[current_pos][action];

		bool moving_with_ball = (move_a && a_has_ball_)
			|| (!move_a && !a_has_ball_);

		if(target_pos == opponent_pos)
		{
			if(moving_with_ball){
				a_has_ball_ = !a_has_ball_;
			}
		}
		else if(target_pos == target_goal || target_pos == opponent_target_goal)
		{
			if(moving_with_ball){
				float reward = (target_pos == target_goal)
					? goal_reward_
					: -goal_reward_;
				if(target_pos == goal_a_){
					state_ = TERMINAL_A_INDEX;
				}
				else{
					state_ = TERMINAL_B_INDEX;
				}
				return reward;
			}
		}
		else
		{ // we have not scored nor moved into our opponent, so update the position
			if(move_a){
				pos_a_ = target_pos;
			}
			else{
				pos_b_ = target_pos;
			}
		}
		state_ = stateMap_[encodeState()];
		return 0; // no reward unless you put the ball in a goal
	}

	// return the reward for player A, while modifying the state. Ordering matters here.
	int SoccerArena::moveAB(bool move_a_first, int act_a, int act_b){
		int reward_a;

		if(move_a_first){
			reward_a = move(true,act_a);
			if(state_ >= N_ACTIVE_STATES){ // if we hit a terminal state
				return reward_a;
			}
		}

		reward_a = -move(false,act_b);
		if(state_ >= N_ACTIVE_STATES){ // if we hit a terminal state
			return reward_a;
		}

		if(!move_a_first){
			reward_a = move(true,act_a);
		}

		return reward_a;
	}
	int SoccerArena::Reset(){
		game_over_ = false;
		game_count_++;
		pos_a_ = 1;
		pos_b_ = 3;
		if(debug_){
			cout << "====initial:=====" << endl;
			cout << Display();
		}
		return GetState();
	}

	int SoccerArena::GetState(){
		int stateHash = encodeState(
			a_has_ball_,
			pos_a_,
			pos_b_
		);
		return stateMap_[stateHash];
	}

	void SoccerArena::SetupByStateID(int stateID){
		int componentIndex = stateMap_[stateID];
		return SetupByComponentIndex(componentIndex);
	}
	void SoccerArena::SetupByComponentIndex(int componentIndex){
		const int *components = states_by_components[componentIndex];
		a_has_ball_ = (components[0] == 1)
			? true
			: false;
		pos_a_ = components[1];
		pos_b_ = components[2];
	}

	string SoccerArena::Display(){
		string graphic = "";
		string positions = "[pos: (" + to_string(pos_a_) + ", " + to_string(pos_b_) + ")]";
		if(debug_){
			cout << "state?: " << DumpState().str();
			cout << "state?: " << GetState() << "; ";
		}

		const int index_map[] = {0,1,2,5,0,3,4,5};

		string a_goal = "[ a ]";
		string b_goal = "[ b ]";

		//              "(10) ";
		string spacer = "     ";
		int state_id = GetState();
		string state_name = (state_id < 10)
			? "(" + to_string(state_id) + ")  "
			: "(" + to_string(state_id) + ") ";
		graphic += state_name;

		for(int i = 0; i < 8; i++){
			if(index_map[i] == 0)
				graphic += a_goal;
			else if(index_map[i] == 5)
				graphic += b_goal;
			else {
				graphic += "[ ";
				if(index_map[i] == pos_a_){
					graphic += "A";
					graphic += (a_has_ball_)
						? "*]"
						: " ]";
				}
				else if(index_map[i] == pos_b_){
					graphic += "B";
					graphic += (!a_has_ball_)
						? "*]"
						: " ]";
				}
				else {
					graphic += "  ]";
				}
			}

			if((i+1)%4 == 0)
				graphic += "\n";
			if(i==3)
				graphic += spacer;
		}
		graphic += "-------------------------\n";
		return graphic;
	}

}; // end namespace

#endif /* SOCCER_ARENA_CC */
