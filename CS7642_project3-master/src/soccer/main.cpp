#include <iostream>
#include <vector>
//#include "soccer_arena.h"
#include "soccer_arena.cc"
#include "player.h"
#include "util.h"

using namespace std;


int testSequence[][2] = {
	{1,0},
	{0,1},
	{1,0},
	{0,1},
	{1,2},
	{2,1},
	{2,0},
	{2,1},
	{1,2},
	{2,1},
	{2,0},
	{2,1},
};
int main(int, char**){
	srand(10); // use random number seed
	bool debug_mode = false;
	cout << randBool() << endl;
	cout << "starting program" << endl;
	Player p1 = Player("test");
	cout << p1.getName() << endl;
	SA::SoccerArena arena = SA::SoccerArena("testArena",1,2,true, debug_mode);
	arena.SetupByStateID(1);
	cout << arena.Display();
	arena.SetupByStateID(35);
	cout << arena.Display();
	//cout << arena.reciteStates();
	cout << "games played: " << arena.getGameCount() << endl;
	for(int state_id = 0; state_id < SA::N_STATES; state_id++){
		cout << "stm summary (" << state_id << "):" << endl;
		cout << arena.summarizeStateTransitionMap(state_id).str() << endl;
	}
	
	cout << arena.Display();
	cout << arena.Display();
	arena.SetupByComponentIndex(19);
	cout << "Just set state to 19 (componentid)" << endl;
	cout << arena.Display();
	cout << arena.DumpState().str() << endl;
	int reward = arena.move(true,3);
	cout << "expect reward to exist: " << reward << endl;
	cout << arena.DumpState().str() << endl;
	cout << arena.encodeState(false,1,2) << endl;

	cout << "state 0 transitions:" << endl;
	arena.SetupByComponentIndex(0);
	cout << arena.Display();
	cout << arena.summarizeStateTransitionMap(0).str() << endl;
	cout << "state 1 transitions:" << endl;
	arena.SetupByComponentIndex(1);
	cout << arena.Display();
	cout << arena.summarizeStateTransitionMap(1).str() << endl;

	int state_id = 0;
	int action = SA::EAST;
	arena.SetupByComponentIndex(state_id);
	cout << arena.Display();
	reward = arena.move(false, action);
	cout << "reward: " << reward << endl;
	cout << "state_: " << arena.GetInnerState() << endl;
	cout << "reward at 0, bFirstReward, EAST (expect 100): " << arena.GetTransition(0).b_first_reward[SA::NORTH][SA::EAST] << endl;

	arena.SetupByComponentIndex(0);
	cout<<arena.Display();
	int rr = arena.moveAB(false,SA::SOUTH,SA::EAST);
	cout << "Rew: " << rr << endl;
	cout<<arena.Display();
	return 0;
}
