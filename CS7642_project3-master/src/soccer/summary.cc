#include <iostream>
#include <vector>
#include "soccer_arena.cc"
#include "player.h"
#include "util.h"

using namespace std;


int main(int, char**){
	srand(10); // use random number seed
	bool debug_mode = false;
	SA::SoccerArena arena = SA::SoccerArena("testArena",1,2,true, debug_mode);
	for(int state_id = 0; state_id < SA::N_ACTIVE_STATES; state_id++){
		arena.SetupByComponentIndex(state_id);
		//cout << "state " << state_id << ":" << endl;
		cout << arena.Display();
		//cout << arena.summarizeStateTransitionMap(state_id).str() << endl;
	}
	
	return 0;
}

