Mitchell Kelley

CS7642_project3
Project 3


```bash
# Choose one of the three "apps" to run:
#APP=ceq
#APP=friend
APP=regularq
cd src/apps/$APP
make
```

Dependencies
- g++
- make
- Coin-OR Clp (open source linear program solver)
- python3

